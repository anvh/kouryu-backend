FROM node:11.4-alpine

WORKDIR /usr/src/app

COPY ./package.json ./yarn.lock ./

RUN yarn
# Bundle app source
COPY ./ .

CMD [ "node", "." ]

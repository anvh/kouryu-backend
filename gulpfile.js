const gulp = require('gulp');
const rename = require('gulp-rename');

// coffee service
gulp.task('coffeeService', function() {
  gulp
    .src('./config/models/model-config.coffee-service.js')
    .pipe(rename('model-config.production.js')) // Rename file
    .pipe(gulp.dest('./server/'));
});

// review service
gulp.task('reviewService', function() {
  gulp
    .src('./config/models/model-config.review-service.js')
    .pipe(rename('model-config.production.js')) // Rename file
    .pipe(gulp.dest('./server/'));
});

#!/bin/bash
aws s3 cp s3://smartlog-build-config/smartlog-stmv3-backend.env .env
DOCKER_LOGIN=`aws ecr get-login --no-include-email --region ap-southeast-1`
${DOCKER_LOGIN}

REGISTRY_URL=744004065806.dkr.ecr.ap-southeast-1.amazonaws.com/smartlog-stmv3-backend
echo "docker build -t ${REGISTRY_URL}:v_${BUILD_NUMBER} --pull=true ${WORKSPACE}"
docker build -t ${REGISTRY_URL}:v_${BUILD_NUMBER} --pull=true ${WORKSPACE}
echo "docker tag ${REGISTRY_URL}:v_${BUILD_NUMBER} ${REGISTRY_URL}:latest"
docker tag ${REGISTRY_URL}:v_${BUILD_NUMBER} ${REGISTRY_URL}:latest
echo "docker push ${REGISTRY_URL}:v_${BUILD_NUMBER}"
docker push ${REGISTRY_URL}:v_${BUILD_NUMBER}
docker push ${REGISTRY_URL}

REGION=ap-southeast-1
REPOSITORY_NAME=smartlog-stmv3-backend
CLUSTER=tf-smartlog
targetGroupArn=arn:aws:elasticloadbalancing:ap-southeast-1:744004065806:targetgroup/test-smartlog-stmv3-backend/b47c84718b963999
containerPort=3000
FAMILY=`sed -n 's/.*"family": "\(.*\)",/\1/p' taskdef.json`
NAME=`sed -n 's/.*"name": "\(.*\)",/\1/p' taskdef.json`
SERVICE_NAME=${NAME}-service
echo "family $FAMILY"
echo "NAME $NAME"
echo "service name $SERVICE_NAME"

#Store the repositoryUri as a variable
REPOSITORY_URI=`aws ecr describe-repositories --repository-names ${REPOSITORY_NAME} --region ${REGION} | jq .repositories[].repositoryUri | tr -d '"'`
echo "respository uri $REPOSITORY_URI"
echo "build number $BUILD_NUMBER"
#Replace the build number and respository URI placeholders with the constants above
sed -e "s;%BUILD_NUMBER%;${BUILD_NUMBER};g" -e "s;%REPOSITORY_URI%;${REPOSITORY_URI};g" taskdef.json > ${NAME}-v_${BUILD_NUMBER}.json
#Register the task definition in the repository
aws ecs register-task-definition --family ${FAMILY} --cli-input-json file://${WORKSPACE}/${NAME}-v_${BUILD_NUMBER}.json --region ${REGION}
SERVICES=`aws ecs describe-services --services ${SERVICE_NAME} --cluster ${CLUSTER} --region ${REGION} | jq .services[].status`

#Get latest revision
REVISION=`aws ecs describe-task-definition --task-definition ${NAME} --region ${REGION} | jq .taskDefinition.revision`

echo "revision aws ecs describe-task-definition --task-definition ${NAME} --region ${REGION} | jq .taskDefinition.revision"
#Create or update service
echo "service $SERVICES"
if [ "$SERVICES" = "\"ACTIVE\"" ]; then
  echo "entered existing service"
  DESIRED_COUNT=`aws ecs describe-services --services ${SERVICE_NAME} --cluster ${CLUSTER} --region ${REGION} | jq .services[].desiredCount`
  if [ ${DESIRED_COUNT} = "0" ]; then
    DESIRED_COUNT="1"
  fi
  aws ecs update-service --cluster ${CLUSTER} --region ${REGION} --service ${SERVICE_NAME} --task-definition ${FAMILY}:${REVISION} --desired-count ${DESIRED_COUNT}

else

  echo "entered new service"
  echo "aws ecs create-service --service-name ${SERVICE_NAME} --desired-count 1 --task-definition ${FAMILY} --cluster ${CLUSTER} --region ${REGION}"
  aws ecs create-service --service-name ${SERVICE_NAME} --desired-count 1 --task-definition ${FAMILY} --cluster ${CLUSTER} --region ${REGION} --load-balancers targetGroupArn=${targetGroupArn},containerName=${NAME},containerPort=${containerPort}

fi

docker rmi ${REGISTRY_URL}:v_${BUILD_NUMBER}

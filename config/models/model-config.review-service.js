module.exports = {
  _meta: {
    sources: [
      'loopback/common/models',
      'loopback/server/models',
      '../common/models',
    ],
    mixins: [
      'loopback/common/mixins',
      'loopback/server/mixins',
      '../common/mixins',
      './mixins',
    ],
  },
  Review: {
    dataSource: 'mongoDs',
    public: true,
  },
  Reviewer: {
    dataSource: 'mongoDs',
    public: true,
  },
};

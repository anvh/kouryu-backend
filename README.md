# kouryu-backend
1) Install node & npm

2) Create a database named "kouryu" in mysql

3) Check out branch "develop"
```
git checkout develop
```

4) Create .env file & setting information
```
cp .env.example .env
```

5) Run app
```
$ npm install
$ node start
```

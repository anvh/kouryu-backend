const config = require('../config/config');

module.exports = {
  mysqlDs: {
    host: config.mysqlHost,
    port: config.mysqlPort,
    database: config.mysqlDatabase,
    username: config.mysqlUser,
    password: config.mysqlPassword,
    name: 'mysqlDs',
    connector: 'mysql',
    maxDepthOfQuery: 20,
  },
};

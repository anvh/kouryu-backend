

require('dotenv').config();

const loopback = require('loopback');
const boot = require('loopback-boot');
const bodyParser = require('body-parser');
const multer = require('multer');
const path = require('path');
const uuid = require('uuid');
const logger = require('../config/logger');
const { isNaN } = require('../common/libraries/utils/type');

const app = loopback();
app.use(loopback.static(path.resolve(__dirname, '../uploads')));
app.use(bodyParser.json());

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, './uploads');
  },
  filename(req, file, cb) {
    cb(null, uuid.v4());
  },
});
app.use(multer({ storage }).any());

module.exports = app;

app.start = async () => app.listen(async () => {
  app.emit('started');
  const baseUrl = app.get('url').replace(/\/$/, '');
  console.log('Web server listening at: %s', baseUrl);
  if (app.get('loopback-component-explorer')) {
    const explorerPath = app.get('loopback-component-explorer').mountPath;
    console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
  }
  try {
    const {
      Action, Role, Account, Industry, Category,
    } = app.models;
    const actionsNum = await Role.count();
    if (!isNaN(actionsNum) && actionsNum < 1) {
      await Action.initActions();
    }
    const rolesNum = await Role.count();
    if (!isNaN(rolesNum) && rolesNum < 1) {
      await Role.initRoles();
    }
    const accountsNum = await Account.count();
    if (!isNaN(accountsNum) && accountsNum < 1) {
      await Account.addAdmin();
    }
    const industriesNum = await Industry.count();
    if (!isNaN(industriesNum) && industriesNum < 1) {
      await Industry.initIndustries();
    }
    const categoriesNum = await Category.count();
    if (!isNaN(categoriesNum) && categoriesNum < 1) {
      await Category.initCategories();
    }
  } catch (error) {
    logger.error({
      message: 'initData',
      errorInfo: `${error}`,
      errorStack: `${error.stack}`,
    });
  }
});

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, (err) => {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) { app.start(); }
});

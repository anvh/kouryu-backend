module.exports = {
  _meta: {
    sources: [
      'loopback/common/models',
      'loopback/server/models',
      '../common/models',
      './models',
      '../common/models/auth',
      '../common/models/company',
      '../common/models/news',
    ],
    mixins: [
      'loopback/common/mixins',
      'loopback/server/mixins',
      '../common/mixins',
      './mixins',
    ],
  },
};

module.exports = async (app) => {
  const { mysqlDs } = app.dataSources;
  const migrate = model => new Promise((resolve, reject) => {
    try {
      mysqlDs.isActual(model, (err, actual) => {
        if (actual) {
          console.log(`Model ${model} is up-to-date. No auto-migrated.`);
          resolve();
        } else {
          console.log(`Difference found! Auto-migrating model ${model}...`);
          mysqlDs.autoupdate(model, () => {
            console.log(`Auto-migrated model ${model} successfully.`);
            resolve(1);
          });
        }
      });
    } catch (error) {
      resolve(1);
    }
  });

  const beginmigrate = async () => {
    for (const model in app.models) {
      await migrate(model);
    }
  };
  await beginmigrate();
};

const config = require('../../config/config');

module.exports = function (app) {
  const log = (context) => {
    const auditLog = {};
    const { method, req } = context;

    auditLog.method = req.method;
    auditLog.url = req.originalUrl;
    auditLog.eventName = method.sharedClass.name;
    auditLog.subEventName = method.name;
    auditLog.service = config.serviceName;
    auditLog.arguments = {
      params: req.params,
      query: req.query,
      // headers: req.headers,
      args: context.args,
    };

    // remove request and response because of size and circular references
    if (auditLog.arguments.args.req) {
      delete auditLog.arguments.args.req;
    }
    if (auditLog.arguments.args.res) {
      delete auditLog.arguments.args.res;
    }

    // use loopbacks toJSON to remove circular references from models
    auditLog.result = Array.isArray(context.result)
      ? context.result.map(entry => (entry && entry.toJSON ? entry.toJSON() : {}))
      : ((context.result && context.result.toJSON) ? context.result.toJSON() : {});
    auditLog.error = context.error || {};
    auditLog.status = context.error
      ? (context.error.statusCode || context.error.status || context.res.statusCode)
      : (context.res.statusCode || 500);
    // var currentUser = {};
    try {
      console.log('auditlog', JSON.stringify(auditLog));
    } catch (error) {
      console.log('auditlog, can not use JSON.stringify()');
    }
  };

  const models = app.models();
  models.forEach((Model) => {
    Model.afterRemote('**', (context, unused, next) => {
      log(context);
      next();
    });
    Model.afterRemoteError('**', (context, next) => {
      log(context);
      next();
    });
  });
};

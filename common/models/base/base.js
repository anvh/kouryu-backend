const loopback = require('loopback');

const ds = loopback.createDataSource('memory');

const ErrorModel = {
  statusCode: Number,
  name: String,
  message: String,
};
ds.define('ErrorModel', ErrorModel, { idInjection: false });

const CreateResponseModel = {
  createdId: String,
  error: 'ErrorModel',
};
ds.define('CreateResponseModel', CreateResponseModel, { idInjection: false });

const UpdateResponseModel = {
  updatedNum: Number,
  error: 'ErrorModel',
};
ds.define('UpdateResponseModel', UpdateResponseModel, { idInjection: false });

const DeleteResponseModel = {
  deletedNum: Number,
  error: 'ErrorModel',
};
ds.define('DeleteResponseModel', DeleteResponseModel, { idInjection: false });

const app = require('../../../../server/server');


const createVerification = async (data) => {
  // accountId, token, type
  const verificationData = await app.models.Verification.create({
    accountId: data.accountId,
    verificationToken: data.verificationToken,
    type: data.type,
  });
  return verificationData;
};

module.exports = {
  createVerification,
};

const { ForbiddenAccessError } = require('smartlog-library');
const { getQueryFilter } = require('../../../libraries/utils/query');
const { isObject } = require('../../../libraries/utils/type');
const { createError } = require('../../../libraries/utils/error');
const { keys: translationKeys } = require('../../../i18n/resources');
const { getTranslation } = require('../../../libraries/utils/translation');
const { checkAdmin } = require('../../../libraries/middleware/auth');

const beforeFinding = (context, unused, next) => {
  const { req } = context || {};
  const { params, user } = req || {};
  const { id } = params || {};
  const { accountId } = user || {};
  if (accountId !== id && !checkAdmin(user)) {
    next(createError(getTranslation(translationKeys.accountInfoForbiddenError), 'ForbiddenAccessError', ForbiddenAccessError));
    return;
  }
  const filter = getQueryFilter(context);
  if (isObject(filter)) {
    filter.include = [
      {
        relation: 'roles',
        scope: {
          fields: ['id', 'name'],
          include: [
            {
              relation: 'actions',
              scope: {
                fields: ['id', 'name'],
              },
            },
          ],
        },
      },
    ];
    filter.fields = ['id', 'name', 'username', 'status', 'createdAt', 'updatedAt'];
    if (!filter.order) {
      filter.order = 'createdAt DESC';
    }
  }
  next();
};

module.exports = {
  beforeFinding,
};

const jwt = require('jsonwebtoken');
const { ApiValidateError, ForbiddenAccessError } = require('smartlog-library');
const { jwtKey } = require('../../../../config/config');
const { isObject } = require('../../../libraries/utils/type');
const { getTranslation } = require('../../../libraries/utils/translation');
const { createError } = require('../../../libraries/utils/error');
const { keys: translationKeys } = require('../../../i18n/resources');
const { accountStatuses } = require('../../../constants');
const sha256 = require('../../../libraries/utils/hash');
const app = require('../../../../server/server');

const login = async (data) => {
  const { username, password } = isObject(data) ? data : {};
  if (!username || !password) {
    throw createError(getTranslation(translationKeys.accountLoginMissingInfoError), 'ApiValidateError', ApiValidateError);
  }
  const account = await app.models.Account.findOne({
    where: { username },
    include: [
      {
        relation: 'roles',
        scope: {
          fields: ['id', 'name'],
          include: [
            {
              relation: 'actions',
              scope: {
                fields: ['id', 'name'],
              },
            },
          ],
        },
      },
    ],
    fields: ['id', 'name', 'username', 'password', 'status'],
  });
  if (!isObject(account)) {
    throw createError(getTranslation(translationKeys.accountLoginInvalidInfoError), 'ApiValidateError', ApiValidateError);
  }
  const hashedPassword = account.password;
  if (hashedPassword !== sha256(password)) {
    throw createError(getTranslation(translationKeys.accountLoginInvalidInfoError), 'ApiValidateError', ApiValidateError);
  }
  const { status } = account;
  if (status === accountStatuses.UNVERIFIED) {
    throw createError(getTranslation(translationKeys.accountUnverifiedError), 'ForbiddenAccessError', ForbiddenAccessError);
  }
  if (status === accountStatuses.BLOCKED) {
    throw createError(getTranslation(translationKeys.accountBlockedError), 'ForbiddenAccessError', ForbiddenAccessError);
  }
  const dataKey = '__data';
  const roles = isObject(account[dataKey]) ? account[dataKey].roles : undefined || (await account.roles.find());
  const accountInfo = {
    accountId: account.id,
    name: account.name,
    username,
    roles: Array.isArray(roles) ? roles.reduce((accumulator, role) => {
      const name = isObject(role) ? role.name : undefined;
      if (name) {
        const actions = isObject(role[dataKey]) ? role[dataKey].actions : undefined || [];
        accumulator[name] = Array.isArray(actions) ? actions.map(action => action.name) : [];
      }
      return accumulator;
    }, {}) : {},
  };
  const accessToken = jwt.sign({ data: accountInfo }, jwtKey, {
    expiresIn: 86400 * 365000, // expires in 1000 years (TODO: refreshToken)
  });
  return {
    data: {
      ...accountInfo,
      accessToken,
    },
  };
};

module.exports = {
  login,
};

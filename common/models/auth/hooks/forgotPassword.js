const jwt = require('jsonwebtoken');
const { ApiValidateError, ForbiddenAccessError } = require('smartlog-library');
const { jwtKey } = require('../../../../config/config');
const { keys: translationKeys } = require('../../../i18n/resources');
const { getTranslation } = require('../../../libraries/utils/translation');
const { isObject } = require('../../../libraries/utils/type');
const { createError } = require('../../../libraries/utils/error');
const { sendEmail } = require('./sendEmail');
const { createVerification } = require('./createVerification');
const { frontendHost, frontendPort, expirationTimeResetPassword } = require('../../../../config/config');
const { accountStatuses, verificationTypes } = require('../../../constants');

const { FORGOTPASSWORD } = verificationTypes;
const { replaceById } = require('./replaceAccountById');
const sha256 = require('../../../libraries/utils/hash');
const app = require('../../../../server/server');

const beforeReseting = (context, unused, next) => {
  const { args, req } = context || {};
  const { options } = args || {};
  if (isObject(options) && isObject(req)) {
    options.user = req.user;
  }
  next();
};

const forgotPassword = async (data) => {
  const { Account, Verification } = app.models;
  const { email } = data || {};
  const account = await Account.findOne({
    where: { username: email },
    include: [
      {
        relation: 'roles',
        scope: {
          fields: ['id', 'name'],
          include: [
            {
              relation: 'actions',
              scope: {
                fields: ['id', 'name'],
              },
            },
          ],
        },
      },
    ],
    fields: ['id', 'name', 'username', 'password', 'status'],
  });
  if (!isObject(account)) {
    throw createError(getTranslation(translationKeys.accountNotFoundError, { accountId: email }), 'ForbiddenAccessError', ForbiddenAccessError);
  }
  const { status } = account;
  if (status === accountStatuses.UNVERIFIED) {
    throw createError(getTranslation(translationKeys.accountUnverifiedError), 'ForbiddenAccessError', ForbiddenAccessError);
  }
  if (status === accountStatuses.BLOCKED) {
    throw createError(getTranslation(translationKeys.accountBlockedError), 'ForbiddenAccessError', ForbiddenAccessError);
  }
  const verification = await Verification.findOne({
    where: { accountId: account.id },
    fields: ['id', 'accountId', 'verificationToken', 'type'],
  });
  if (isObject(verification) && verification.type === FORGOTPASSWORD) {
    const token = verification.verificationToken;
    try {
      jwt.verify(token, jwtKey, async (err, decoded) => {
        if (isObject(err)) {
          const { message } = err;
          if (message === 'jwt expired') {
            await Verification.destroyAll({
              where: { accoundId: account.id, type: FORGOTPASSWORD },
            });
          }
        } else {
          throw createError(getTranslation(translationKeys.emailHasBeenSentError), 'ApiValidateError', ApiValidateError);
        }
      });
    } catch (error) {
      throw (error);
    }
  }

  const dataKey = '__data';
  const roles = isObject(account[dataKey]) ? account[dataKey].roles : undefined || (await account.roles.find());
  const accountInfo = {
    accountId: account.id,
    name: account.name,
    username: email,
    roles: Array.isArray(roles) ? roles.reduce((accumulator, role) => {
      const name = isObject(role) ? role.name : undefined;
      if (name) {
        const actions = isObject(role[dataKey]) ? role[dataKey].actions : undefined || [];
        accumulator[name] = Array.isArray(actions) ? actions.map(action => action.name) : [];
      }
      return accumulator;
    }, {}) : {},
  };
  const tokenString = jwt.sign({ data: accountInfo }, jwtKey, {
    expiresIn: expirationTimeResetPassword * 60,
  });

  const verificationData = await createVerification({
    accountId: account.id,
    verificationToken: tokenString,
    type: FORGOTPASSWORD,
  });
  if (!isObject(verificationData)) {
    return;
  }
  const link = `http://${frontendHost}:${frontendPort}/auth/confirm?token=${tokenString}`;
  const subject = getTranslation(translationKeys.emailForgotPasswordSubject);
  const htmlContent = getTranslation(translationKeys.emailForgotPasswordHtml, { email: `${email}`, link: `${link}`, time: `${expirationTimeResetPassword}` });
  sendEmail(email, subject, htmlContent);
};

const resetPassword = async (data) => {
  const { token, password } = isObject(data) ? data : {};
  const decoded = jwt.decode(token);
  const accountInfo = isObject(decoded) && decoded.data ? decoded.data : {};

  if (!isObject(accountInfo)) {
    throw createError(getTranslation(translationKeys.verificationTokenInvalidError), 'ForbiddenAccessError', ForbiddenAccessError);
  }
  const object = {
    password: sha256(password),
  };
  const options = {
    user: accountInfo,
  };

  const result = await replaceById(token, accountInfo.accountId, object, options);
  if (!isObject(result) || result.updatedNum <= 0) {
    throw createError(getTranslation(translationKeys.accountUpdateError), 'ForbiddenAccessError', ForbiddenAccessError);
  }
  const { Verification } = app.models;
  Verification.destroyAll({ where: { accoundId: accountInfo.id, type: FORGOTPASSWORD } });

  const accessToken = jwt.sign({ data: accountInfo }, jwtKey, {
    expiresIn: 86400 * 365000, // expires in 1000 years (TODO: refreshToken)
  });

  return {
    data: {
      ...accountInfo,
      accessToken,
    },
  };
};

module.exports = {
  beforeReseting,
  forgotPassword,
  resetPassword,
};

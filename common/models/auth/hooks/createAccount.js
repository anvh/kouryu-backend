const { ApiValidateError } = require('smartlog-library');
const { isObject } = require('../../../libraries/utils/type');
const { createError } = require('../../../libraries/utils/error');
const { keys: translationKeys } = require('../../../i18n/resources');
const { getTranslation } = require('../../../libraries/utils/translation');
const { minAccountPasswordLength } = require('../../../../config/config');
const constants = require('../../../constants');
const sha256 = require('../../../libraries/utils/hash');
const app = require('../../../../server/server');
const verifyAccount = require('./verifyAccount');

const create = async (data) => {
  const { password } = data || {};
  if (!password || password < minAccountPasswordLength) {
    throw createError(getTranslation(translationKeys.accountPasswordTooShortError), 'ApiValidateError', ApiValidateError);
  }
  let createdAccountId = null;
  await app.dataSources.mysqlDs.transaction(async (models) => {
    const { Account, Role, AccountRole } = models;
    const userRole = await Role.findOne({
      where: { name: constants.roles.USER },
      fields: ['id'],
    });
    const userRoleId = isObject(userRole) ? userRole.id : undefined;
    if (!userRoleId) {
      return;
    }
    Object.assign(data, {
      password: sha256(password),
      status: constants.accountStatuses.UNVERIFIED,
    });
    const createdAccount = await Account.originalCreate(data).catch((error) => {
      const errorMsg = error.message || `${error}`;
      if (errorMsg && errorMsg.includes('ER_DUP_ENTRY')) {
        throw createError(getTranslation(translationKeys.accountDuplicateError), 'ApiValidateError', ApiValidateError);
      }
      throw error;
    });
    if (!isObject(createdAccount)) {
      return;
    }
    createdAccountId = createdAccount.id;
    await AccountRole.create({
      accountId: createdAccountId,
      roleId: userRoleId,
    });
    verifyAccount.preprocessToSendEmail(data, createdAccountId);
  });
  return { createdId: createdAccountId };
};

module.exports = {
  create,
};

const { ApiValidateError, ForbiddenAccessError } = require('smartlog-library');
const { isObject } = require('../../../libraries/utils/type');
const { createError } = require('../../../libraries/utils/error');
const { keys: translationKeys } = require('../../../i18n/resources');
const { getTranslation } = require('../../../libraries/utils/translation');
const { checkAdmin } = require('../../../libraries/middleware/auth');
const app = require('../../../../server/server');

const beforeReplacing = (context, unused, next) => {
  const { args, req } = context || {};
  const { options } = args || {};
  if (isObject(options) && isObject(req)) {
    options.user = req.user;
  }
  next();
};

const replaceById = async (token, id, data, options) => {
  if (!isObject(data)) {
    throw createError(getTranslation(translationKeys.accountNoDataError), 'ApiValidateError', ApiValidateError);
  }
  const { user } = options || {};
  if (!checkAdmin(user)) {
    if (!isObject(user) || user.accountId !== id) {
      throw createError(getTranslation(translationKeys.accountUnauthorizedError), 'ForbiddenAccessError', ForbiddenAccessError);
    }
    if (data.status !== undefined) {
      throw createError(getTranslation(translationKeys.accountStatusUpdatingForbiddenError), 'ForbiddenAccessError', ForbiddenAccessError);
    }
  }
  const { Account } = app.models;
  const account = await Account.findById(id || '', {
    fields: ['username', 'password', 'status'],
  });
  if (!isObject(account)) {
    throw createError(getTranslation(translationKeys.accountNotFoundError, { accountId: id }), 'ForbiddenAccessError', ForbiddenAccessError);
  }
  const result = await Account.updateAll({ id }, data);
  return { updatedNum: isObject(result) ? result.count : null };
};

module.exports = {
  beforeReplacing,
  replaceById,
};

const { isObject } = require('../../../libraries/utils/type');
const app = require('../../../../server/server');

const beforeCreating = (context, unused, next) => {
  const { args, req } = context || {};
  const { options } = args || {};
  if (isObject(options) && isObject(req)) {
    options.user = req.user;
  }
  next();
};

const create = async (data, options) => app.models.Action.originalCreate(data, options);

module.exports = {
  beforeCreating,
  create,
};

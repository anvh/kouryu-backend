const { ForbiddenAccessError } = require('smartlog-library');
const { isObject } = require('../../../libraries/utils/type');
const { createError } = require('../../../libraries/utils/error');
const { keys: translationKeys } = require('../../../i18n/resources');
const { getTranslation } = require('../../../libraries/utils/translation');
const { checkAdmin } = require('../../../libraries/middleware/auth');
const app = require('../../../../server/server');

const beforeDeleting = (context, unused, next) => {
  const { args, req } = context || {};
  const { options } = args || {};
  if (isObject(options) && isObject(req)) {
    options.user = req.user;
  }
  next();
};

const deleteById = async (id, options) => {
  const { user } = options || {};
  if (!checkAdmin(user)) {
    throw createError(getTranslation(translationKeys.accountUnauthorizedError), 'ForbiddenAccessError', ForbiddenAccessError);
  }
  const { Account } = app.models;
  const account = await Account.findById(id || '');
  if (!isObject(account)) {
    throw createError(getTranslation(translationKeys.accountNotFoundError, { accountId: id }), 'ForbiddenAccessError', ForbiddenAccessError);
  }
  await Account.originalDeleteById(id, options);
  return { message: getTranslation(translationKeys.accountDeleteSuccess) };
};

module.exports = {
  beforeDeleting,
  deleteById,
};

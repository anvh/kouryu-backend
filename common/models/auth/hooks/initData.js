const app = require('../../../../server/server');
const sha256 = require('../../../libraries/utils/hash');
const { actions, roles, accountStatuses } = require('../../../constants');
const { isObject } = require('../../../libraries/utils/type');

const initActions = async () => {
  await app.dataSources.mysqlDs.transaction(async (models) => {
    const { Action } = models;
    const promises = [];
    promises.push(Action.originalCreate({ name: actions.READ_ACCOUNTS }));
    promises.push(Action.originalCreate({ name: actions.READ_ACCOUNT }));
    promises.push(Action.originalCreate({ name: actions.CREATE_STAFF_ACCOUNT }));
    promises.push(Action.originalCreate({ name: actions.CREATE_ADMIN_ACCOUNT }));
    promises.push(Action.originalCreate({ name: actions.UPDATE_ACCOUNT }));
    promises.push(Action.originalCreate({ name: actions.DELETE_ACCOUNT }));

    promises.push(Action.originalCreate({ name: actions.READ_COMPANIES }));
    promises.push(Action.originalCreate({ name: actions.READ_COMPANY }));
    promises.push(Action.originalCreate({ name: actions.CREATE_COMPANY }));
    promises.push(Action.originalCreate({ name: actions.UPDATE_COMPANY }));
    promises.push(Action.originalCreate({ name: actions.DELETE_COMPANY }));

    promises.push(Action.originalCreate({ name: actions.CREATE_NEWS }));
    promises.push(Action.originalCreate({ name: actions.UPDATE_NEWS }));
    promises.push(Action.originalCreate({ name: actions.DELETE_NEWS }));
    await Promise.all(promises);
  });
};

const initRoles = async () => {
  await app.dataSources.mysqlDs.transaction(async (models) => {
    const { Role, Action, RoleAction } = models;
    let promises = [];
    promises.push(Role.originalCreate({ name: roles.ADMIN }));
    promises.push(Role.originalCreate({ name: roles.STAFF }));
    promises.push(Role.originalCreate({ name: roles.USER }));
    const createdRoles = await Promise.all(promises);
    const rolesMap = Array.isArray(createdRoles) ? createdRoles.reduce((accumulator, role) => {
      if (isObject(role)) {
        const { id, name } = role;
        if (id && name) {
          accumulator[name] = id;
        }
      }
      return accumulator;
    }, {}) : undefined;
    if (!isObject(rolesMap)) {
      return;
    }
    const createdActions = await Action.find({ fields: ['id', 'name'] });
    const actionsMap = Array.isArray(createdActions) ? createdActions.reduce((accumulator, action) => {
      if (isObject(action)) {
        const { id, name } = action;
        if (id && name) {
          accumulator[name] = id;
        }
      }
      return accumulator;
    }, {}) : undefined;
    if (!isObject(actionsMap)) {
      return;
    }
    promises = [];
    const adminRoleId = rolesMap[roles.ADMIN];
    if (adminRoleId) {
      const actionIds = createdActions.map(action => action.id);
      const actionIdsNum = actionIds.length;
      for (let i = 0; i < actionIdsNum; i += 1) {
        const actionId = actionIds[i];
        if (actionId) {
          promises.push(RoleAction.create({
            roleId: adminRoleId,
            actionId,
          }));
        }
      }
    }
    const staffRoleId = rolesMap[roles.STAFF];
    if (staffRoleId) {
      [
        actions.READ_ACCOUNT, actions.UPDATE_ACCOUNT,
        actions.READ_COMPANIES, actions.READ_COMPANY,
        actions.CREATE_COMPANY, actions.UPDATE_COMPANY,
        actions.DELETE_COMPANY,
      ].forEach((actionName) => {
        const actionId = actionsMap[actionName];
        if (actionId) {
          promises.push(RoleAction.create({
            roleId: staffRoleId,
            actionId,
          }));
        }
      });
    }
    const userRoleId = rolesMap[roles.USER];
    if (userRoleId) {
      [
        actions.READ_ACCOUNT, actions.UPDATE_ACCOUNT,
        actions.READ_COMPANIES, actions.READ_COMPANY,
        actions.CREATE_COMPANY, actions.UPDATE_COMPANY,
        actions.DELETE_COMPANY, actions.CREATE_NEWS,
        actions.UPDATE_NEWS, actions.DELETE_NEWS,
      ].forEach((actionName) => {
        const actionId = actionsMap[actionName];
        if (actionId) {
          promises.push(RoleAction.create({
            roleId: userRoleId,
            actionId,
          }));
        }
      });
    }
    await Promise.all(promises);
  });
};

const addAdmin = async () => {
  await app.dataSources.mysqlDs.transaction(async (models) => {
    const { Account, Role, AccountRole } = models;
    const adminRole = await Role.findOne({
      where: { name: roles.ADMIN },
      fields: ['id'],
    });
    const adminRoleId = isObject(adminRole) ? adminRole.id : undefined;
    if (adminRoleId) {
      const admin = await Account.originalCreate({
        username: 'admin',
        password: sha256('123456789'),
        status: accountStatuses.VERIFIED,
      });
      await AccountRole.create({
        accountId: admin.id,
        roleId: adminRoleId,
      });
    }
  });
};

module.exports = {
  initActions,
  initRoles,
  addAdmin,
};

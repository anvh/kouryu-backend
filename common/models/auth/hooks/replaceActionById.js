const { ApiValidateError, ForbiddenAccessError } = require('smartlog-library');
const { isObject } = require('../../../libraries/utils/type');
const { createError } = require('../../../libraries/utils/error');
const { keys: translationKeys } = require('../../../i18n/resources');
const { getTranslation } = require('../../../libraries/utils/translation');
const { checkAdmin } = require('../../../libraries/middleware/auth');
const app = require('../../../../server/server');

const beforeReplacing = (context, unused, next) => {
  const { args, req } = context || {};
  const { options } = args || {};
  if (isObject(options) && isObject(req)) {
    options.user = req.user;
  }
  next();
};

const replaceById = async (id, data, options) => {
  if (!isObject(data)) {
    throw createError(getTranslation(translationKeys.accountNoDataError), 'ApiValidateError', ApiValidateError);
  }
  const { user } = options || {};
  if (!checkAdmin(user)) {
    throw createError(getTranslation(translationKeys.accountUnauthorizedError), 'ForbiddenAccessError', ForbiddenAccessError);
  }
  const { Account } = app.models;
  const account = await Account.findById(id || '', {
    fields: ['username', 'password'],
  });
  if (!isObject(account)) {
    throw createError(getTranslation(translationKeys.accountNotFoundError, { accountId: id }), 'ForbiddenAccessError', ForbiddenAccessError);
  }
  Object.assign(account, data);
  await Account.originalReplaceById(id, account, options);
  return { message: getTranslation(translationKeys.accountUpdateSuccess) };
};

module.exports = {
  beforeReplacing,
  replaceById,
};

const nodemailer = require('nodemailer');
const { ApiValidateError } = require('smartlog-library');
const { keys: translationKeys } = require('../../../i18n/resources');
const { getTranslation } = require('../../../libraries/utils/translation');
const { createError } = require('../../../libraries/utils/error');
const { emailService, emailAccount, emailPassword } = require('../../../../config/config');


const sendEmail = async (email, emailSubject, emailHtmlContent) => {
  const smtpTransport = nodemailer.createTransport({
    service: emailService,
    auth: {
      user: emailAccount,
      pass: emailPassword,
    },
  });

  const mailOptions = {
    from: 'noreply@hayabusa.com',
    to: email,
    subject: emailSubject,
    text: 'Verify Email',
    html: emailHtmlContent,
  };

  smtpTransport.sendMail(mailOptions, (error, info) => {
    if (error) {
      const errorMsg = getTranslation(translationKeys.emailVerificationSendError, { message: error.message || `${error}` });
      throw createError(errorMsg, error.name, ApiValidateError);
    }

    smtpTransport.close();
  });
};

module.exports = {
  sendEmail,
};

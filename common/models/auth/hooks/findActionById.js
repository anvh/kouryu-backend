const { getQueryFilter } = require('../../../libraries/utils/query');
const { isObject } = require('../../../libraries/utils/type');

const beforeFinding = (context, unused, next) => {
  const filter = getQueryFilter(context);
  if (isObject(filter)) {
    filter.fields = ['username', 'password'];
  }
  next();
};

module.exports = {
  beforeFinding,
};

const { getQueryFilter } = require('../../../libraries/utils/query');
const { isObject } = require('../../../libraries/utils/type');

const beforeFinding = (context, unused, next) => {
  const filter = getQueryFilter(context);
  if (isObject(filter)) {
    filter.include = [
      {
        relation: 'roles',
        scope: {
          fields: ['id', 'name'],
          include: [
            {
              relation: 'actions',
              scope: {
                fields: ['id', 'name'],
              },
            },
          ],
        },
      },
    ];
    filter.fields = ['id', 'name', 'username', 'status', 'createdAt', 'updatedAt'];
    if (!filter.order) {
      filter.order = 'createdAt DESC';
    }
  }
  next();
};

module.exports = {
  beforeFinding,
};

const jwt = require('jsonwebtoken');
const uuidv4 = require('uuid/v4');
const { ApiValidateError, ForbiddenAccessError } = require('smartlog-library');
const { jwtKey } = require('../../../../config/config');
const { keys: translationKeys } = require('../../../i18n/resources');
const { getTranslation } = require('../../../libraries/utils/translation');
const { isObject } = require('../../../libraries/utils/type');
const { createError } = require('../../../libraries/utils/error');
const { sendEmail } = require('./sendEmail');
const { createVerification } = require('./createVerification');
const { frontendHost, frontendPort } = require('../../../../config/config');
const { accountStatuses, verificationTypes } = require('../../../constants');
const app = require('../../../../server/server');

const beforeVerifying = (context, unused, next) => {
  const { args, req } = context || {};
  const { options } = args || {};
  if (isObject(options) && isObject(req)) {
    options.user = req.user;
  }
  next();
};

const preprocessToSendEmail = async (data, accountId) => {
  const { username } = data || {};
  const verificationTokenString = uuidv4();
  const verificationData = await createVerification({
    accountId,
    verificationToken: verificationTokenString,
    type: verificationTypes.VERIFICATION,
  });
  if (!isObject(verificationData)) {
    return;
  }
  const link = `http://${frontendHost}:${frontendPort}/auth/verify?token=${verificationTokenString}`;
  const subject = getTranslation(translationKeys.emailVerificationSubject);
  const htmlContent = getTranslation(translationKeys.emailVerificationHtml, { email: `${username}`, link: `${link}` });
  sendEmail(username, subject, htmlContent);
};

const verify = async (data) => {
  const { token } = data || {};
  if (!token) {
    throw createError(getTranslation(translationKeys.verificationTokenNullError), 'ApiValidateError', ApiValidateError);
  }

  let accountInfo;
  await app.dataSources.mysqlDs.transaction(async (models) => {
    const { Account, Verification } = models;
    const verification = await Verification.findOne({
      where: { verificationToken: token, type: verificationTypes.VERIFICATION },
      fields: ['id', 'accountId', 'verificationToken'],
    });
    if (!verification) {
      throw createError(getTranslation(translationKeys.verificationTokenInvalidError), 'ApiValidateError', ApiValidateError);
    }
    const account = await Account.findOne({
      where: { id: verification.accountId },
      include: [
        {
          relation: 'roles',
          scope: {
            fields: ['id', 'name'],
            include: [
              {
                relation: 'actions',
                scope: {
                  fields: ['id', 'name'],
                },
              },
            ],
          },
        },
      ],
      fields: ['id', 'name', 'username', 'password', 'status'],
    });
    if (!isObject(account)) {
      throw createError(getTranslation(translationKeys.accountNotFoundError, { accountId: verification.accountId }), 'ForbiddenAccessError', ForbiddenAccessError);
    }
    const { status } = account;
    if (status === accountStatuses.BLOCKED) {
      throw createError(getTranslation(translationKeys.accountBlockedError), 'ForbiddenAccessError', ForbiddenAccessError);
    }
    let result;
    if (status === accountStatuses.UNVERIFIED) {
      result = await Account.updateAll({ id: account.id }, { status: accountStatuses.VERIFIED });
    }
    if (isObject(result) || status === accountStatuses.VERIFIED) {
      Verification.destroyAll({ where: { id: verification.id, type: verificationTypes.verification } });
    }

    const dataKey = '__data';
    const roles = isObject(account[dataKey]) ? account[dataKey].roles : undefined || (await account.roles.find());
    accountInfo = {
      accountId: account.id,
      name: account.name,
      username: account.username,
      roles: Array.isArray(roles) ? roles.map(role => role.name) : [],
    };
    accountInfo = {
      accountId: account.id,
      name: account.name,
      username: account.username,
      roles: Array.isArray(roles) ? roles.reduce((accumulator, role) => {
        const name = isObject(role) ? role.name : undefined;
        if (name) {
          const actions = isObject(role[dataKey]) ? role[dataKey].actions : undefined || [];
          accumulator[name] = Array.isArray(actions) ? actions.map(action => action.name) : [];
        }
        return accumulator;
      }, {}) : {},
    };
  });
  const accessToken = jwt.sign({ data: accountInfo }, jwtKey, {
    expiresIn: 86400 * 365000, // expires in 1000 years (TODO: refreshToken)
  });

  return {
    data: {
      ...accountInfo,
      accessToken,
    },
  };
};
module.exports = {
  beforeVerifying,
  verify,
  preprocessToSendEmail,
};

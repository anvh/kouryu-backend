const loopback = require('loopback');

require('../base/base');

const ds = loopback.createDataSource('memory');
const { createSchema, updateSchema } = require('./role-validation');
const { authRoles } = require('../../libraries/middleware/auth');
const { validateBody } = require('../../libraries/middleware/validate');
const constants = require('../../constants');
const createHook = require('./hooks/createAction');
const replaceByIdHook = require('./hooks/replaceActionById');
const deleteByIdHook = require('./hooks/deleteActionById');
const findHook = require('./hooks/findActions');
const findByIdHook = require('./hooks/findActionById');
const initDataHook = require('./hooks/initData');

const UpsertActionModel = {
  name: String,
  description: String,
};
ds.define('UpsertActionModel', UpsertActionModel, { idInjection: false });

const ActionModel = {
  id: String,
  name: String,
  description: String,
  createdAt: Date,
  updatedAt: Date,
};
ds.define('RoleModel', ActionModel, { idInjection: false });

module.exports = (Action) => {
  Action.beforeRemote('create', authRoles(constants.roles.ADMIN));
  Action.beforeRemote('create', validateBody(createSchema));
  Action.beforeRemote('create', createHook.beforeCreating);

  Action.beforeRemote('replaceById', authRoles(constants.roles.ADMIN));
  Action.beforeRemote('replaceById', validateBody(updateSchema));
  Action.beforeRemote('replaceById', replaceByIdHook.beforeReplacing);

  Action.beforeRemote('deleteById', authRoles(constants.roles.ADMIN));
  Action.beforeRemote('deleteById', deleteByIdHook.beforeDeleting);

  Action.beforeRemote('find', authRoles(constants.roles.ADMIN));
  Action.beforeRemote('find', findHook.beforeFinding);

  Action.beforeRemote('findById', authRoles(constants.roles.ADMIN));
  Action.beforeRemote('findById', findByIdHook.beforeFinding);

  Action.once('attached', () => {
    Object.assign(Action, {
      create: createHook.create,
      originalCreate: Action.create,
      replaceById: replaceByIdHook.replaceById,
      originalReplaceById: Action.replaceById,
      deleteById: deleteByIdHook.deleteById,
      originalDeleteById: Action.deleteById,
    });
  });

  Object.assign(Action, { initActions: initDataHook.initActions });
};

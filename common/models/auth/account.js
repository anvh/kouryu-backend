const loopback = require('loopback');

require('../base/base');

const ds = loopback.createDataSource('memory');
const {
  createSchema,
  updateSchema,
  loginSchema,
  forgotPasswordSchema,
} = require('./account-validation');
const { authActions } = require('../../libraries/middleware/auth');
const { validateBody } = require('../../libraries/middleware/validate');
const { actions } = require('../../constants');
const createHook = require('./hooks/createAccount');
const replaceByIdHook = require('./hooks/replaceAccountById');
const deleteByIdHook = require('./hooks/deleteAccountById');
const findHook = require('./hooks/findAccounts');
const findByIdHook = require('./hooks/findAccountById');
const loginHook = require('./hooks/login');
const initDataHook = require('./hooks/initData');
const forgotPasswordHook = require('./hooks/forgotPassword');

const UpsertAccountModel = {
  name: String,
  username: String,
  password: String,
};
ds.define('UpsertAccountModel', UpsertAccountModel, { idInjection: false });

const AccountModel = {
  id: String,
  name: String,
  username: String,
  status: String,
  createdAt: Date,
  updatedAt: Date,
};
ds.define('AccountModel', AccountModel, { idInjection: false });

const LoginModel = {
  username: String,
  password: String,
};
ds.define('LoginModel', LoginModel, { idInjection: false });

const LoggedInAccountInfoModel = {
  accountId: String,
  name: String,
  username: String,
  roles: [String],
  accessToken: String,
};
ds.define('LoggedInAccountInfoModel', LoggedInAccountInfoModel, { idInjection: false });

const LoginResponseModel = {
  data: 'LoggedInAccountInfoModel',
  error: 'ErrorModel',
};
ds.define('LoginResponseModel', LoginResponseModel, { idInjection: false });

const EmailModel = {
  email: String,
};
ds.define('EmailModel', EmailModel, { idInjection: false });

const ResetPasswordModel = {
  token: String,
  password: String,
};
ds.define('ResetPasswordModel', ResetPasswordModel, { idInjection: false });

module.exports = (Account) => {
  Account.beforeRemote('create', validateBody(createSchema));

  Account.beforeRemote('replaceById', authActions(actions.UPDATE_ACCOUNT));
  Account.beforeRemote('replaceById', validateBody(updateSchema));
  Account.beforeRemote('replaceById', replaceByIdHook.beforeReplacing);

  Account.beforeRemote('deleteById', authActions(actions.DELETE_ACCOUNT));
  Account.beforeRemote('deleteById', deleteByIdHook.beforeDeleting);

  Account.beforeRemote('find', authActions(actions.READ_ACCOUNTS));
  Account.beforeRemote('find', findHook.beforeFinding);

  Account.beforeRemote('findById', authActions(actions.READ_ACCOUNT));
  Account.beforeRemote('findById', findByIdHook.beforeFinding);

  Account.once('attached', () => {
    Object.assign(Account, {
      create: createHook.create,
      originalCreate: Account.create,
      replaceById: replaceByIdHook.replaceById,
      originalReplaceById: Account.replaceById,
      deleteById: deleteByIdHook.deleteById,
      originalDeleteById: Account.deleteById,
    });
  });

  Object.assign(Account, { login: loginHook.login });
  Account.beforeRemote('login', validateBody(loginSchema));

  Object.assign(Account, { forgotPassword: forgotPasswordHook.forgotPassword });
  Account.beforeRemote('forgotPassword', validateBody(forgotPasswordSchema));
  Object.assign(Account, { resetPassword: forgotPasswordHook.resetPassword });
  Account.beforeRemote('resetPassword', forgotPasswordHook.beforeReseting);

  Object.assign(Account, { addAdmin: initDataHook.addAdmin });
};

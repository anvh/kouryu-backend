const Joi = require('joi');
const { minAccountPasswordLength } = require('../../../config/config');
const { accountStatuses } = require('../../constants');

const createSchema = Joi.object({
  name: Joi.string().allow(null, '').optional(),
  username: Joi.string().email().required(),
  password: Joi.string().min(minAccountPasswordLength).required(),
});

const updateSchema = Joi.object({
  name: Joi.string().allow(null, '').optional(),
  username: Joi.string().email().allow(null, '').optional(),
  password: Joi.string().min(minAccountPasswordLength).optional(),
  status: Joi.string().valid(
    accountStatuses.UNVERIFIED,
    accountStatuses.VERIFIED,
    accountStatuses.BLOCKED,
  ).optional(),
}).or('name', 'username', 'password', 'status');

const loginSchema = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().required(),
});

const verifySchema = Joi.object({
  token: Joi.string().required(),
});

const forgotPasswordSchema = Joi.object({
  email: Joi.string().required(),
});


module.exports = {
  createSchema,
  updateSchema,
  loginSchema,
  verifySchema,
  forgotPasswordSchema,
};

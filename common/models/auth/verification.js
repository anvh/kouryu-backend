const loopback = require('loopback');

require('../base/base');

const ds = loopback.createDataSource('memory');
const {
  verifySchema,
} = require('./account-validation');
const { validateBody } = require('../../libraries/middleware/validate');
const verifyAccountHook = require('./hooks/verifyAccount');


const UpsertVerificationModel = {
  verificationId: String,
  verificationToken: String,
};
ds.define('UpsertVerificationModel', UpsertVerificationModel, { idInjection: false });

const VerificationModel = {
  verificationId: String,
  accountId: String,
  verificationToken: String,
};
ds.define('VerificationModel', VerificationModel, { idInjection: false });

const VerificationTokenModel = {
  token: String,
};
ds.define('VerificationTokenModel', VerificationTokenModel, { idInjection: false });


module.exports = (Verification) => {
  Object.assign(Verification, { verify: verifyAccountHook.verify });
  Verification.beforeRemote('verify', validateBody(verifySchema));
};

const loopback = require('loopback');

require('../base/base');

const ds = loopback.createDataSource('memory');
const { createSchema, updateSchema } = require('./role-validation');
const { authRoles } = require('../../libraries/middleware/auth');
const { validateBody } = require('../../libraries/middleware/validate');
const constants = require('../../constants');
const createHook = require('./hooks/createRole');
const replaceByIdHook = require('./hooks/replaceRoleById');
const deleteByIdHook = require('./hooks/deleteRoleById');
const findHook = require('./hooks/findRoles');
const findByIdHook = require('./hooks/findRoleById');
const initDataHook = require('./hooks/initData');

const UpsertRoleModel = {
  name: String,
  description: String,
};
ds.define('UpsertRoleModel', UpsertRoleModel, { idInjection: false });

const RoleModel = {
  id: String,
  name: String,
  description: String,
  createdAt: String,
  updatedAt: String,
};
ds.define('RoleModel', RoleModel, { idInjection: false });

module.exports = (Role) => {
  Role.beforeRemote('create', authRoles(constants.roles.ADMIN));
  Role.beforeRemote('create', validateBody(createSchema));
  Role.beforeRemote('create', createHook.beforeCreating);

  Role.beforeRemote('replaceById', authRoles(constants.roles.ADMIN));
  Role.beforeRemote('replaceById', validateBody(updateSchema));
  Role.beforeRemote('replaceById', replaceByIdHook.beforeReplacing);

  Role.beforeRemote('deleteById', authRoles(constants.roles.ADMIN));
  Role.beforeRemote('deleteById', deleteByIdHook.beforeDeleting);

  Role.beforeRemote('find', authRoles(constants.roles.ADMIN));
  Role.beforeRemote('find', findHook.beforeFinding);

  Role.beforeRemote('findById', authRoles(constants.roles.ADMIN));
  Role.beforeRemote('findById', findByIdHook.beforeFinding);

  Role.once('attached', () => {
    Object.assign(Role, {
      create: createHook.create,
      originalCreate: Role.create,
      replaceById: replaceByIdHook.replaceById,
      originalReplaceById: Role.replaceById,
      deleteById: deleteByIdHook.deleteById,
      originalDeleteById: Role.deleteById,
    });
  });

  Object.assign(Role, { initRoles: initDataHook.initRoles });
};

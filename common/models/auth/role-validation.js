const Joi = require('joi');

const createSchema = Joi.object({
  name: Joi.string().required(),
  description: Joi.string().max(1000).allow(null, '').optional(),
});

const updateSchema = Joi.object({
  name: Joi.string().allow(null, '').optional(),
  description: Joi.string().allow(null, '').optional(),
}).or('name', 'description');

module.exports = {
  createSchema,
  updateSchema,
};

const Joi = require('joi');
const { newsStatuses } = require('../../constants');

const createSchema = Joi.object({
  title: Joi.string().required(),
  brief: Joi.string().required(),
  description: Joi.string().required(),
  location: Joi.string().required(),
  categories: Joi.array().items(Joi.string()).required(),
  startDate: Joi.string().required(),
  endDate: Joi.string().required(),
  companyId: Joi.string().required(),
  accountId: Joi.string().required(),
});

const updateSchema = Joi.object({
  title: Joi.string().required(),
  brief: Joi.string().required(),
  description: Joi.string().required(),
  location: Joi.string().required(),
  categories: Joi.array().items(Joi.string()).required(),
  startDate: Joi.string().required(),
  endDate: Joi.string().required(),
  companyId: Joi.string().required(),
  accountId: Joi.string().required(),
  status: Joi.string().valid(
    newsStatuses.PRIVATE,
    newsStatuses.PUBLIC,
    newsStatuses.BLOCKED,
    newsStatuses.DELETED,
  ).optional(),
});

module.exports = {
  createSchema,
  updateSchema,
};

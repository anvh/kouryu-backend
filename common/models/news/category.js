const loopback = require('loopback');

require('../base/base');

const ds = loopback.createDataSource('memory');
const initDataHook = require('./hooks/initData');
const getCategoriesHook = require('./hooks/getCategories');

const UpsertIndustryModel = {
  name: String,
  description: String,
};
ds.define('UpsertIndustryModel', UpsertIndustryModel, { idInjection: false });

module.exports = (Category) => {
  Category.once('attached', () => {
    Object.assign(Category, {
      originalCreate: Category.create,
      get: getCategoriesHook.getCategories,
    });
  });

  Object.assign(Category, { initCategories: initDataHook.initCategories });
};

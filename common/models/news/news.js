const loopback = require('loopback');

require('../base/base');

const ds = loopback.createDataSource('memory');
const {
  createSchema,
  updateSchema,
} = require('./news-validation');
const { actions } = require('../../constants');
const { authActions } = require('../../libraries/middleware/auth');
const { validateBody } = require('../../libraries/middleware/validate');
const createHook = require('./hooks/createNews');
const getNewsListByAccountIdHook = require('./hooks/getNewsListByAccountId');
const getNewsListHook = require('./hooks/getNewsList');
const findByIdHook = require('./hooks/findNewsById');
const searchHook = require('./hooks/searchNews');
const replaceByIdHook = require('./hooks/replaceNewsById');
const deleteByIdHook = require('./hooks/deleteNewsById');

const UpsertNewsModel = {
  title: String,
  brief: String,
  description: String,
  location: String,
  categories: [String],
  startDate: Date,
  endDate: Date,
  companyId: String,
  accountId: String,
};
ds.define('UpsertNewsModel', UpsertNewsModel, { idInjection: false });

const NewsModel = {
  title: String,
  brief: String,
  description: String,
  location: String,
  categories: [String],
  startDate: Date,
  endDate: Date,
  companyId: String,
  accountId: String,
  status: String,
};
ds.define('NewsModel', NewsModel, { idInjection: false });

module.exports = (News) => {
  News.beforeRemote('create', validateBody(createSchema));
  News.beforeRemote('create', authActions(actions.CREATE_NEWS));
  News.beforeRemote('create', createHook.beforeCreating);
  News.beforeRemote('getNewsListByAccountId', getNewsListByAccountIdHook.beforeFinding);
  News.beforeRemote('getNewsList', getNewsListHook.beforeFinding);
  News.beforeRemote('replaceById', authActions(actions.UPDATE_NEWS));
  News.beforeRemote('replaceById', validateBody(updateSchema));
  News.beforeRemote('replaceById', replaceByIdHook.beforeReplacing);
  News.beforeRemote('deleteById', authActions(actions.DELETE_NEWS));
  News.beforeRemote('deleteById', deleteByIdHook.beforeDeleting);

  News.once('attached', () => {
    Object.assign(News, {
      create: createHook.create,
      originalCreate: News.create,
      getNewsListByAccountId: getNewsListByAccountIdHook.getNewsList,
      getNewsList: getNewsListHook.getNewsList,
      findById: findByIdHook.findNewsById,
      search: searchHook.search,
      replaceById: replaceByIdHook.replaceById,
      originalReplaceById: News.replaceById,
      deleteById: deleteByIdHook.deleteById,
      originalDeleteById: News.deleteById,
    });
  });
};

const { newsStatuses, companyStatuses } = require('../../../constants');
const app = require('../../../../server/server');
const { checkContains } = require('../../../libraries/utils/contains');

const search = async (keyword, skip, limit, companyId, categories, startDate, endDate) => {
  const skipItems = skip || 0;
  let limitItems = limit || 0;
  const { News } = app.models;
  const news = await News.find({
    order: 'createdAt DESC',
    include: [
      {
        relation: 'categories',
        scope: {
          fields: ['id', 'name'],
        },
      },
      {
        relation: 'company',
        scope: {
          fields: ['id', 'name', 'address', 'phoneNumber', 'taxNumber', 'foundingDate', 'description', 'logo', 'accountId', 'status'],
          include: [
            {
              relation: 'account',
              scope: {
                fields: ['name', 'username'],
              },
            },
          ],
        },
      },
    ],
    where: {
      status: { inq: [newsStatuses.PRIVATE, newsStatuses.PUBLIC] },
      and: [
        companyId ? {
          companyId,
        } : {},
        {
          or: [
            {
              title: {
                regexp: `/${keyword}/i`,
              },
            },
            {
              brief: {
                regexp: `/${keyword}/i`,
              },
            },
            {
              description: {
                regexp: `/${keyword}/i`,
              },
            },
          ],
        },
        startDate ? { startDate: { gte: startDate } } : {},
        endDate ? { endDate: { lte: endDate } } : {},
      ],
    },
    fields: ['id', 'title', 'brief', 'description', 'location', 'startDate', 'endDate', 'companyId', 'accountId', 'status', 'company'],
  }).filter(obj => obj.toJSON().company.status === companyStatuses.VERIFIED)
    .filter(obj => (categories ? checkContains(obj.toJSON().categories, categories) : true));

  const data = [];
  if (limitItems === 0) {
    limitItems = news.length;
  }
  for (let i = skipItems; i < Math.min(skipItems + limitItems, news.length); i += 1) {
    const { company } = news[i].toJSON();
    if (company.status === companyStatuses.VERIFIED) {
      data.push(news[i]);
    }
  }
  return {
    data: {
      total: news.length,
      skip: skip || 0,
      limit: limit || 0,
      news: data,
    },
  };
};

module.exports = {
  search,
};

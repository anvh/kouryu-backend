const app = require('../../../../server/server');

const getCategories = async () => {
  const categories = await app.models.Category.find({
    fields: ['id', 'name', 'description'],
  });
  return {
    data: {
      categories,
    },
  };
};

module.exports = {
  getCategories,
};

const { isObject } = require('../../../libraries/utils/type');
const { newsStatuses, companyStatuses } = require('../../../constants');
const app = require('../../../../server/server');

const beforeFinding = (context, unused, next) => {
  const { args, req } = context || {};
  const { options } = args || {};
  if (isObject(options) && isObject(req)) {
    options.user = req.user;
  }
  next();
};

const getNewsList = async (companyId, skip, limit) => {
  const skipItems = skip || 0;
  let limitItems = limit || 0;
  const { News } = app.models;
  const news = await News.find({
    order: 'createdAt DESC',
    include: [
      {
        relation: 'categories',
        scope: {
          fields: ['id', 'name'],
        },
      },
      {
        relation: 'company',
        scope: {
          fields: ['id', 'name', 'address', 'phoneNumber', 'taxNumber', 'foundingDate', 'description', 'logo', 'accountId', 'status'],
          include: [
            {
              relation: 'account',
              scope: {
                fields: ['name', 'username'],
              },
            },
          ],
        },
      },
    ],
    where: {
      or: [
        {
          companyId,
          status: { inq: [newsStatuses.PRIVATE, newsStatuses.PUBLIC] },
        },
      ],
    },
    fields: ['id', 'title', 'brief', 'description', 'location', 'startDate', 'endDate', 'companyId', 'accountId', 'status', 'company'],
  }).filter(obj => obj.toJSON().company.status === companyStatuses.VERIFIED);

  const data = [];
  if (limitItems === 0) {
    limitItems = news.length;
  }
  for (let i = skipItems; i < Math.min(skipItems + limitItems, news.length); i += 1) {
    const { company } = news[i].toJSON();
    if (company.status === companyStatuses.VERIFIED) {
      data.push(news[i]);
    }
  }
  return {
    data: {
      total: news.length,
      skip: skip || 0,
      limit: limit || 0,
      news: data,
    },
  };
};

module.exports = {
  beforeFinding,
  getNewsList,
};

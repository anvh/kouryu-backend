const { ApiValidateError } = require('smartlog-library');
const { isObject } = require('../../../libraries/utils/type');
const { keys: translationKeys } = require('../../../i18n/resources');
const { getTranslation } = require('../../../libraries/utils/translation');
const { createError } = require('../../../libraries/utils/error');
const { newsStatuses } = require('../../../constants');
const app = require('../../../../server/server');

const beforeCreating = (context, unused, next) => {
  const { args, req } = context || {};
  const { options } = args || {};
  if (isObject(options) && isObject(req)) {
    options.user = req.user;
  }
  next();
};

const create = async (data) => {
  const { categories } = data;
  // eslint-disable-next-line no-param-reassign
  data.categories = undefined;
  const newsInfo = {
    ...data,
    status: newsStatuses.PUBLIC,
  };

  let createdNewsId = null;
  const promises = [];
  await app.dataSources.mysqlDs.transaction(async (models) => {
    const {
      News, NewsCategory, Category, Company, Account,
    } = models;
    const companyInfo = await Company.findOne({
      where: { id: newsInfo.companyId },
    });
    if (!isObject(companyInfo)) {
      throw createError(getTranslation(translationKeys.companyIdInvalidError), 'ApiValidateError', ApiValidateError);
    }
    const accountInfo = await Account.findOne({
      where: { id: newsInfo.accountId },
    });
    if (!isObject(accountInfo)) {
      throw createError(getTranslation(translationKeys.accountNotFoundError), 'ApiValidateError', ApiValidateError);
    }
    const createdNews = await News.originalCreate(newsInfo).catch((error) => {
      const errorMsg = error.message || `${error}`;
      if (errorMsg) {
        throw createError(getTranslation(translationKeys.newsCreateError), 'ApiValidateError', ApiValidateError);
      }
      throw error;
    });
    if (!isObject(createdNews)) {
      throw createError(getTranslation(translationKeys.newsCreateError), 'ApiValidateError', ApiValidateError);
    }
    createdNewsId = createdNews.id;
    JSON.parse(categories).forEach(async (categoryId) => {
      const category = await Category.findOne({
        where: { id: categoryId },
        fields: ['id', 'name'],
      });
      if (isObject(category)) {
        promises.push(NewsCategory.create({
          newsId: createdNewsId,
          categoryId: category.id,
        }));
      }
    });
    await Promise.all(promises);
  });
  return {
    createdId: createdNewsId,
  };
};


module.exports = {
  beforeCreating,
  create,
};

const { ForbiddenAccessError } = require('smartlog-library');
const { isObject } = require('../../../libraries/utils/type');
const { createError } = require('../../../libraries/utils/error');
const { keys: translationKeys } = require('../../../i18n/resources');
const { getTranslation } = require('../../../libraries/utils/translation');
const { checkAdmin, checkStaff } = require('../../../libraries/middleware/auth');
const { newsStatuses } = require('../../../constants');
const app = require('../../../../server/server');

const beforeDeleting = (context, unused, next) => {
  const { args, req } = context || {};
  const { options } = args || {};
  if (isObject(options) && isObject(req)) {
    options.user = req.user;
  }
  next();
};

const deleteById = async (id, options) => {
  const { user } = options || {};
  const { News, Company } = app.models;
  const news = await News.findById(id || '');
  const { accountId, companyId } = news.data.newsInfo[0] || {};
  const company = await Company.findById(companyId || '');
  if (!isObject(news)) {
    throw createError(getTranslation(translationKeys.companyNotFoundError), 'ForbiddenAccessError', ForbiddenAccessError);
  }
  if (!checkAdmin(user) && !checkStaff(user)) {
    if (user.accountId !== accountId && user.accountId !== company.data.companyInfo[0].accountId) {
      throw createError(getTranslation(translationKeys.accountUnauthorizedError), 'ForbiddenAccessError', ForbiddenAccessError);
    }
  }
  const result = await News.updateAll({ id }, { status: newsStatuses.DELETED });
  return { deletedNum: isObject(result) ? result.count : null };
};

module.exports = {
  beforeDeleting,
  deleteById,
};

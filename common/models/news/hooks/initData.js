const app = require('../../../../server/server');

const initCategories = async () => {
  await app.dataSources.mysqlDs.transaction(async (models) => {
    const { Category } = models;
    const promises = [];
    promises.push(Category.originalCreate({ name: 'Architect & Construction' }));
    promises.push(Category.originalCreate({ name: 'Events' }));
    promises.push(Category.originalCreate({ name: 'Foods and Drinks' }));
    promises.push(Category.originalCreate({ name: 'Real Estate' }));
    promises.push(Category.originalCreate({ name: 'Sell and Buy' }));
    promises.push(Category.originalCreate({ name: 'Teaching and Education' }));
    await Promise.all(promises);
  });
};

module.exports = {
  initCategories,
};

const { newsStatuses } = require('../../../constants');
const app = require('../../../../server/server');

const findNewsById = async (id) => {
  const newsInfo = await app.models.News.find({
    order: 'createdAt DESC',
    where: {
      or: [
        {
          id,
          status: newsStatuses.PRIVATE,
        },
        {
          id,
          status: newsStatuses.PUBLIC,
        },
      ],
    },
    include: [
      {
        relation: 'categories',
        scope: {
          fields: ['id', 'name'],
        },
      },
    ],
    fields: ['id', 'title', 'brief', 'description', 'location', 'startDate', 'endDate', 'companyId', 'accountId', 'status'],
  });

  return {
    data: {
      newsInfo,
    },
  };
};

module.exports = {
  findNewsById,
};

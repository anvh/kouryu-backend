const { ApiValidateError, ForbiddenAccessError } = require('smartlog-library');
const { isObject } = require('../../../libraries/utils/type');
const { createError } = require('../../../libraries/utils/error');
const { keys: translationKeys } = require('../../../i18n/resources');
const { getTranslation } = require('../../../libraries/utils/translation');
const { checkAdmin, checkStaff } = require('../../../libraries/middleware/auth');
const { newsStatuses } = require('../../../constants');
const app = require('../../../../server/server');

const beforeReplacing = (context, unused, next) => {
  const { args, req } = context || {};
  const { options } = args || {};
  if (isObject(options) && isObject(req)) {
    options.user = req.user;
  }
  next();
};

const compare = async (arr1, arr2) => {
  if (arr1.length === arr2.length && arr1.every((u, i) => u === arr2[i])) {
    return true;
  }
  return false;
};

const compareCategories = async (categories, newsId) => {
  const newsCategories = [];
  const arr = await app.models.NewsCategory.find({
    where: { newsId },
    fields: ['categoryId'],
  });
  arr.forEach(async (item) => {
    const { categoryId } = item || {};
    newsCategories.push(categoryId);
  });
  if (!isObject(newsCategories)) {
    return false;
  }
  if (categories.length === 0) {
    return true;
  }
  return compare(newsCategories.sort(), categories.sort());
};

const replaceById = async (id, data, options) => {
  const {
    News, Company, NewsCategory, Category,
  } = app.models;
  const { categories } = data;
  delete data.categories;
  const newsInfo = {
    id,
    ...data,
  };

  if (!isObject(data)) {
    throw createError(getTranslation(translationKeys.companyNoDataError), 'ApiValidateError', ApiValidateError);
  }
  const { user } = options || {};
  if (!checkAdmin(user) || !checkStaff(user)) {
    if (!isObject(user) || user.accountId !== newsInfo.accountId) {
      throw createError(getTranslation(translationKeys.accountUnauthorizedError), 'ForbiddenAccessError', ForbiddenAccessError);
    }
    if (newsInfo.status !== undefined
      && (newsInfo.status !== newsStatuses.DELETED || newsInfo.status !== newsStatuses.BLOCKED)) {
      throw createError(getTranslation(translationKeys.accountUnauthorizedError), 'ForbiddenAccessError', ForbiddenAccessError);
    }
  }

  const news = await News.findOne({
    order: 'createdAt DESC',
    where: {
      or: [
        {
          id,
          status: newsStatuses.PRIVATE,
        },
        {
          id,
          status: newsStatuses.PUBLIC,
        },
      ],
    },
    fields: ['id', 'title', 'brief', 'description', 'location', 'startDate', 'endDate', 'companyId', 'accountId', 'status'],
  });
  if (!isObject(news)) {
    throw createError(getTranslation(translationKeys.newsNoDataError), 'ForbiddenAccessError', ForbiddenAccessError);
  }
  const company = await Company.findOne({
    where: { id: news.companyId, accountId: newsInfo.accountId },
  });
  if (!isObject(company)) {
    throw createError(getTranslation(translationKeys.accountUnauthorizedError), 'ForbiddenAccessError', ForbiddenAccessError);
  }

  let result;
  await app.dataSources.mysqlDs.transaction(async () => {
    const promises = [];
    const isEqual = await compareCategories(JSON.parse(categories), newsInfo.id);
    if (!isEqual) {
      await NewsCategory.destroyAll({ newsId: newsInfo.id });
      JSON.parse(categories).forEach(async (categoryId) => {
        const category = await Category.findOne({
          where: { id: categoryId },
          fields: ['id', 'name'],
        });
        if (isObject(category)) {
          promises.push(NewsCategory.create({
            newsId: newsInfo.id,
            categoryId: category.id,
          }));
        }
      });
      await Promise.all(promises);
    }
    result = await News.updateAll({ id }, newsInfo);
  });
  return { updatedNum: isObject(result) ? result.count : null };
};

module.exports = {
  beforeReplacing,
  replaceById,
};

const Joi = require('joi');
const { companyStatuses } = require('../../constants');

const createSchema = Joi.object({
  name: Joi.string().required(),
  address: Joi.string().required(),
  industries: Joi.array().items(Joi.string()).required(),
  phoneNumber: Joi.string().required(),
  taxNumber: Joi.string().required(),
  foundingDate: Joi.string().required(),
  description: Joi.string().allow(null, '').optional(),
  logo: Joi.string().allow(null, '').optional(),
  accountId: Joi.string().required(),
});

const updateSchema = Joi.object({
  name: Joi.string().required(),
  address: Joi.string().required(),
  industries: Joi.array().items(Joi.string()).required(),
  phoneNumber: Joi.string().required(),
  taxNumber: Joi.string().required(),
  foundingDate: Joi.string().required(),
  description: Joi.string().allow(null, '').optional(),
  logo: Joi.string().allow(null, '').optional(),
  status: Joi.string().valid(
    companyStatuses.UNVERIFIED,
    companyStatuses.VERIFIED,
    companyStatuses.BLOCKED,
    companyStatuses.DELETED,
  ).optional(),
  accountId: Joi.string().required(),
});

module.exports = {
  createSchema,
  updateSchema,
};

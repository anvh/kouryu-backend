const { ForbiddenAccessError } = require('smartlog-library');
const { createError } = require('../../../libraries/utils/error');
const { keys: translationKeys } = require('../../../i18n/resources');
const { getTranslation } = require('../../../libraries/utils/translation');
const { checkAdmin } = require('../../../libraries/middleware/auth');
const { companyStatuses, newsStatuses } = require('../../../constants');
const app = require('../../../../server/server');

const beforeFinding = (context, unused, next) => {
  const { req } = context || {};
  const { params, user } = req || {};
  const { id } = params || {};
  const { accountId } = user || {};
  if (accountId !== id && !checkAdmin(user)) {
    next(createError(getTranslation(translationKeys.accountInfoForbiddenError), 'ForbiddenAccessError', ForbiddenAccessError));
    return;
  }
  next();
};

const getCompaniesByAccountId = async (token, id, skip, limit) => {
  const total = await app.models.Company.count(
    { accountId: id, status: companyStatuses.VERIFIED },
  );
  const companies = await app.models.Company.find({
    order: 'createdAt DESC',
    limit,
    skip,
    where: { accountId: id, status: companyStatuses.VERIFIED },
    include: [
      {
        relation: 'industries',
        scope: {
          fields: ['id', 'name'],
        },
      },
    ],
    fields: ['id', 'name', 'address', 'phoneNumber', 'taxNumber', 'foundingDate', 'description', 'logo', 'accountId', 'status'],
  }).map(async (company) => {
    const news = await app.models.News.find({
      where: {
        or: [
          {
            companyId: company.id,
            status: { inq: [newsStatuses.PRIVATE, newsStatuses.PUBLIC] },
          },
        ],
      },
      fields: ['id', 'title'],
    });
    return Object.assign(company, { post: news.length });
  });

  return {
    data: {
      total,
      skip: skip || 0,
      limit: limit || 0,
      companies,
    },
  };
};

module.exports = {
  beforeFinding,
  getCompaniesByAccountId,
};

const { companyStatuses } = require('../../../constants');
const app = require('../../../../server/server');

const findCompanyById = async (id) => {
  const companyInfo = await app.models.Company.find({
    order: 'createdAt DESC',
    where: { id, status: companyStatuses.VERIFIED },
    include: [
      {
        relation: 'industries',
        scope: {
          fields: ['id', 'name'],
        },
      },
    ],
    fields: ['id', 'name', 'address', 'phoneNumber', 'taxNumber', 'foundingDate', 'description', 'logo', 'accountId', 'status'],
  });

  return {
    data: {
      companyInfo,
    },
  };
};

module.exports = {
  findCompanyById,
};

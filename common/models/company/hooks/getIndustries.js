const app = require('../../../../server/server');

const getIndustries = async () => {
  const industries = await app.models.Industry.find({
    fields: ['id', 'name', 'description'],
  });
  return {
    data: {
      industries,
    },
  };
};

module.exports = {
  getIndustries,
};

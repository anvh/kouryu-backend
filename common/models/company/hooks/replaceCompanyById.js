const { ApiValidateError, ForbiddenAccessError } = require('smartlog-library');
const { isObject } = require('../../../libraries/utils/type');
const { createError } = require('../../../libraries/utils/error');
const { keys: translationKeys } = require('../../../i18n/resources');
const { getTranslation } = require('../../../libraries/utils/translation');
const { checkAdmin, checkStaff } = require('../../../libraries/middleware/auth');
const { companyStatuses } = require('../../../constants');
const app = require('../../../../server/server');

const beforeReplacing = (context, unused, next) => {
  const { args, req } = context || {};
  const { options } = args || {};
  if (isObject(req)) {
    if (isObject(options)) {
      options.user = req.user;
    }
    const { body, files } = req;
    const filesNum = Array.isArray(files) ? files.length : 0;
    for (let i = 0; i < filesNum; i += 1) {
      const file = files[i];
      if (isObject(file)) {
        const { path } = file;
        if (path) {
          if (isObject(body)) {
            body.logo = path;
          }
          break;
        }
      }
    }
  }
  next();
};

const compare = async (arr1, arr2) => {
  if (arr1.length === arr2.length && arr1.every((u, i) => u === arr2[i])) {
    return true;
  }
  return false;
};

const compareIndustries = async (industries, companyId) => {
  const companyIndustries = [];
  const arr = await app.models.CompanyIndustry.find({
    where: { companyId },
    fields: ['industryId'],
  });
  arr.forEach(async (item) => {
    const { industryId } = item || {};
    companyIndustries.push(industryId);
  });
  if (!isObject(companyIndustries)) {
    return false;
  }
  return compare(companyIndustries.sort(), JSON.parse(industries).sort());
};

const replaceById = async (id, data, options) => {
  const {
    logo, name, address, industries, phoneNumber, taxNumber, foundingDate, description, accountId,
  } = data;
  const companyInfo = {
    logo: logo === 'null' ? 'uploads\\logo.jpg' : logo,
    name,
    address,
    phoneNumber,
    taxNumber,
    foundingDate,
    description,
    accountId,
    status: companyStatuses.VERIFIED,
  };
  if (!isObject(data)) {
    throw createError(getTranslation(translationKeys.companyNoDataError), 'ApiValidateError', ApiValidateError);
  }
  const { user } = options || {};
  if (!checkAdmin(user) || !checkStaff(user)) {
    if (!isObject(user) || user.accountId !== data.accountId) {
      throw createError(getTranslation(translationKeys.accountUnauthorizedError), 'ForbiddenAccessError', ForbiddenAccessError);
    }
    if (data.status !== undefined && data.status !== companyStatuses.DELETED) {
      throw createError(getTranslation(translationKeys.accountStatusUpdatingForbiddenError), 'ForbiddenAccessError', ForbiddenAccessError);
    }
  }

  const company = await app.models.Company.findById(id || '', {
    fields: ['id', 'name', 'address', 'phoneNumber', 'taxNumber', 'foundingDate', 'description', 'logo', 'accountId', 'status'],
  });
  if (!isObject(company)) {
    throw createError(getTranslation(translationKeys.companyNoDataError), 'ForbiddenAccessError', ForbiddenAccessError);
  }
  let result;
  await app.dataSources.mysqlDs.transaction(async (models) => {
    const { Company, CompanyIndustry, Industry } = models;
    const companyId = company.data.companyInfo[0].id || {};
    const promises = [];
    const isEqual = await compareIndustries(industries, companyId);
    if (!isEqual) {
      await CompanyIndustry.destroyAll({ companyId });
      JSON.parse(industries).forEach(async (industryId) => {
        const industry = await Industry.findOne({
          where: { id: industryId },
          fields: ['id', 'name'],
        });
        if (isObject(industry)) {
          promises.push(CompanyIndustry.create({
            companyId,
            industryId: industry.id,
          }));
        }
      });
      await Promise.all(promises);
    }
    result = await Company.updateAll({ id }, companyInfo);
  });
  return { updatedNum: isObject(result) ? result.count : null };
};

module.exports = {
  beforeReplacing,
  replaceById,
};

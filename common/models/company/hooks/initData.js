const app = require('../../../../server/server');

const initIndustries = async () => {
  await app.dataSources.mysqlDs.transaction(async (models) => {
    const { Industry } = models;
    const promises = [];
    promises.push(Industry.originalCreate({ name: 'Accounting' }));
    promises.push(Industry.originalCreate({ name: 'Agriculture' }));
    promises.push(Industry.originalCreate({ name: 'Architect & Construction' }));
    promises.push(Industry.originalCreate({ name: 'Arts' }));
    promises.push(Industry.originalCreate({ name: 'Capital Markets' }));
    promises.push(Industry.originalCreate({ name: 'Education' }));
    promises.push(Industry.originalCreate({ name: 'Import and Export' }));
    promises.push(Industry.originalCreate({ name: 'IT - Hardware' }));
    promises.push(Industry.originalCreate({ name: 'IT - Software' }));
    promises.push(Industry.originalCreate({ name: 'Media' }));
    promises.push(Industry.originalCreate({ name: 'Real Estate' }));
    await Promise.all(promises);
  });
};

module.exports = {
  initIndustries,
};

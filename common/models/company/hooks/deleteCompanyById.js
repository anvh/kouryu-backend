const { ForbiddenAccessError } = require('smartlog-library');
const { isObject } = require('../../../libraries/utils/type');
const { createError } = require('../../../libraries/utils/error');
const { keys: translationKeys } = require('../../../i18n/resources');
const { getTranslation } = require('../../../libraries/utils/translation');
const { checkAdmin, checkStaff } = require('../../../libraries/middleware/auth');
const { companyStatuses } = require('../../../constants');
const app = require('../../../../server/server');

const beforeDeleting = (context, unused, next) => {
  const { args, req } = context || {};
  const { options } = args || {};
  if (isObject(options) && isObject(req)) {
    options.user = req.user;
  }
  next();
};

const deleteById = async (id, options) => {
  const { user } = options || {};
  const { Company } = app.models;
  const company = await Company.findById(id || '');
  const { accountId } = company.data.companyInfo[0] || {};
  if (!isObject(company)) {
    throw createError(getTranslation(translationKeys.companyNotFoundError), 'ForbiddenAccessError', ForbiddenAccessError);
  }
  if (!checkAdmin(user) && !checkStaff(user)) {
    if (user.accountId !== accountId) {
      throw createError(getTranslation(translationKeys.accountUnauthorizedError), 'ForbiddenAccessError', ForbiddenAccessError);
    }
  }
  const result = await Company.updateAll({ id }, { status: companyStatuses.DELETED });
  return { deletedNum: isObject(result) ? result.count : null };
};

module.exports = {
  beforeDeleting,
  deleteById,
};

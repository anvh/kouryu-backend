const { newsStatuses, companyStatuses } = require('../../../constants');
const app = require('../../../../server/server');
const { checkContains } = require('../../../libraries/utils/contains');

const search = async (keyword, skip, limit, industries) => {
  const skipItems = skip || 0;
  let limitItems = limit || 0;

  const companies = await app.models.Company.find({
    order: 'createdAt DESC',
    where: {
      status: companyStatuses.VERIFIED,
      or: [
        {
          name: {
            regexp: `/${keyword}/i`,
          },
        },
        {
          description: {
            regexp: `/${keyword}/i`,
          },
        },
      ],
    },
    include: [
      {
        relation: 'industries',
        scope: {
          fields: ['id', 'name'],
        },
      },
    ],
    fields: ['id', 'name', 'address', 'phoneNumber', 'taxNumber', 'foundingDate', 'description', 'logo', 'accountId', 'status'],
  }).filter(company => (industries ? checkContains(company.toJSON().industries, industries) : true))
    .map(async (company) => {
      const news = await app.models.News.find({
        where: {
          or: [
            {
              companyId: company.id,
              status: { inq: [newsStatuses.PRIVATE, newsStatuses.PUBLIC] },
            },
          ],
        },
        fields: ['id', 'title'],
      });
      return Object.assign(company, { post: news.length });
    });

  const data = [];
  if (limitItems === 0) {
    limitItems = companies.length;
  }
  for (let i = skipItems; i < Math.min(skipItems + limitItems, companies.length); i += 1) {
    data.push(companies[i]);
  }
  return {
    data: {
      total: companies.length,
      skip: skip || 0,
      limit: limit || 0,
      companies: data,
    },
  };
};

module.exports = {
  search,
};

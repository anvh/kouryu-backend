const { ApiValidateError } = require('smartlog-library');
const { isObject } = require('../../../libraries/utils/type');
const { keys: translationKeys } = require('../../../i18n/resources');
const { getTranslation } = require('../../../libraries/utils/translation');
const { createError } = require('../../../libraries/utils/error');
const { companyStatuses } = require('../../../constants');
const app = require('../../../../server/server');

const beforeCreating = (context, unused, next) => {
  const { args, req } = context || {};
  const { options } = args || {};
  if (isObject(req)) {
    if (isObject(options)) {
      options.user = req.user;
    }
    const { body, files } = req;
    const filesNum = Array.isArray(files) ? files.length : 0;
    for (let i = 0; i < filesNum; i += 1) {
      const file = files[i];
      if (isObject(file)) {
        const { path } = file;
        if (path) {
          if (isObject(body)) {
            body.logo = path;
          }
          break;
        }
      }
    }
  }
  next();
};

const create = async (data) => {
  const {
    logo, name, address, industries, phoneNumber, taxNumber, foundingDate, description, accountId,
  } = data;
  const companyInfo = {
    logo: logo === 'null' ? 'uploads\\logo.jpg' : logo,
    name,
    address,
    phoneNumber,
    taxNumber,
    foundingDate,
    description,
    accountId,
    status: companyStatuses.VERIFIED,
  };

  let createdCompanyId = null;
  const promises = [];
  await app.dataSources.mysqlDs.transaction(async (models) => {
    const { Company, CompanyIndustry, Industry } = models;
    const createdCompany = await Company.originalCreate(companyInfo).catch((error) => {
      const errorMsg = error.message || `${error}`;
      if (errorMsg && errorMsg.includes('ER_DUP_ENTRY')) {
        if (errorMsg.includes('name_addr_phone_unique')) {
          throw createError(getTranslation(translationKeys.companyNameAdressPhoneNumberDuplicateError), 'ApiValidateError', ApiValidateError);
        } else if (errorMsg.includes('taxNumber')) {
          throw createError(getTranslation(translationKeys.companyTaxNumberDuplicateError, { taxNumber: `${taxNumber}` }), 'ApiValidateError', ApiValidateError);
        }
        throw errorMsg;
      }
      throw error;
    });
    if (!isObject(createdCompany)) {
      return;
    }
    createdCompanyId = createdCompany.id;
    JSON.parse(industries).forEach(async (industryId) => {
      const industry = await Industry.findOne({
        where: { id: industryId },
        fields: ['id', 'name'],
      });
      if (isObject(industry)) {
        promises.push(CompanyIndustry.create({
          companyId: createdCompanyId,
          industryId: industry.id,
        }));
      }
    });
    await Promise.all(promises);
  });
  return {
    createdId: createdCompanyId,
  };
};

module.exports = {
  beforeCreating,
  create,
};

const loopback = require('loopback');

require('../base/base');

const ds = loopback.createDataSource('memory');
const initDataHook = require('./hooks/initData');
const getIndustriesHook = require('./hooks/getIndustries');

const UpsertIndustryModel = {
  name: String,
  description: String,
};
ds.define('UpsertIndustryModel', UpsertIndustryModel, { idInjection: false });
// const UpsertVerificationModel = {
//   verificationId: String,
//   verificationToken: String,
// };
// ds.define('UpsertVerificationModel', UpsertVerificationModel, { idInjection: false });

// const VerificationModel = {
//   verificationId: String,
//   accountId: String,
//   verificationToken: String,
// };
// ds.define('VerificationModel', VerificationModel, { idInjection: false });

// const VerificationTokenModel = {
//   token: String,
// };
// ds.define('VerificationTokenModel', VerificationTokenModel, { idInjection: false });

const IndustriesResponseModel = {
  name: String,
  address: String,
  phoneNumber: String,
  taxNumber: String,
  foundingDate: Date,
  description: String,
};

ds.define('IndustriesResponseModel', IndustriesResponseModel, { idInjection: false });

module.exports = (Industry) => {
  Industry.once('attached', () => {
    Object.assign(Industry, {
      // create: createHook.create,
      originalCreate: Industry.create,
      get: getIndustriesHook.getIndustries,
      // replaceById: replaceByIdHook.replaceById,
      // originalReplaceById: Action.replaceById,
      // deleteById: deleteByIdHook.deleteById,
      // originalDeleteById: Action.deleteById,
    });
  });

  Object.assign(Industry, { initIndustries: initDataHook.initIndustries });
};

const loopback = require('loopback');

require('../base/base');

const ds = loopback.createDataSource('memory');
const {
  createSchema,
  updateSchema,
} = require('./company-validation');
const { actions } = require('../../constants');
const { authActions } = require('../../libraries/middleware/auth');
const { validateBody } = require('../../libraries/middleware/validate');
const createHook = require('./hooks/createCompany');
const getCompaniesByAccountIdHook = require('./hooks/getCompaniesByAccountId');
const getCompaniesHook = require('./hooks/getCompanies');
const replaceByIdHook = require('./hooks/replaceCompanyById');
const findByIdHook = require('./hooks/findCompanyById');
const searchHook = require('./hooks/searchCompanies');
const deleteByIdHook = require('./hooks/deleteCompanyById');

const UpsertCompanyModel = {
  name: String,
  address: String,
  industries: [String],
  phoneNumber: String,
  taxNumber: String,
  foundingDate: Date,
  description: String,
  logo: String,
  status: String,
};
ds.define('UpsertCompanyModel', UpsertCompanyModel, { idInjection: false });

const CompanyModel = {
  id: String,
  name: String,
  address: String,
  industries: [String],
  phoneNumber: String,
  taxNumber: String,
  foundingDate: Date,
  description: String,
  logo: String,
  status: String,
  accountId: String,
};
ds.define('CompanyModel', CompanyModel, { idInjection: false });

module.exports = (Company) => {
  Company.beforeRemote('create', validateBody(createSchema));
  Company.beforeRemote('create', authActions(actions.CREATE_COMPANY));
  Company.beforeRemote('create', createHook.beforeCreating);
  Company.beforeRemote('replaceById', authActions(actions.UPDATE_COMPANY));
  Company.beforeRemote('replaceById', validateBody(updateSchema));
  Company.beforeRemote('replaceById', replaceByIdHook.beforeReplacing);
  Company.beforeRemote('getCompaniesByAccountId', authActions(actions.READ_COMPANIES));
  Company.beforeRemote('getCompaniesByAccountId', getCompaniesByAccountIdHook.beforeFinding);
  Company.beforeRemote('getCompanies', getCompaniesHook.beforeFinding);
  Company.beforeRemote('deleteById', authActions(actions.DELETE_COMPANY));
  Company.beforeRemote('deleteById', deleteByIdHook.beforeDeleting);

  Company.once('attached', () => {
    Object.assign(Company, {
      create: createHook.create,
      originalCreate: Company.create,
      replaceById: replaceByIdHook.replaceById,
      originalReplaceById: Company.replaceById,
      getCompaniesByAccountId: getCompaniesByAccountIdHook.getCompaniesByAccountId,
      getCompanies: getCompaniesHook.getCompanies,
      findById: findByIdHook.findCompanyById,
      search: searchHook.search,
      deleteById: deleteByIdHook.deleteById,
      originalDeleteById: Company.deleteById,
    });
  });
};

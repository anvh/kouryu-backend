const {
  permitAction,
  permitRole,
  permitRoleAction,
} = require('smartlog-library');
const TokenError = require('smartlog-library/src/errors/token_error');
const { isObject } = require('../utils/type');
const constants = require('../../constants');

const onAuthorized = (err, req, next) => {
  if (err) {
    next(err);
    return;
  }
  const tokenUser = req.user;
  if (!tokenUser) {
    next(new TokenError('No user in req'));
    return;
  }
  req.user = {
    accountId: tokenUser.accountId,
    roles: tokenUser.roles,
  };
  next();
};

const authActions = (...actions) => (context, unused, next) => {
  const { req, res } = context || {};
  permitAction(...actions)(req, res, err => onAuthorized(err, req, next));
};

const authRoles = (...roles) => (context, unused, next) => {
  const { req, res } = context || {};
  permitRole(...roles)(req, res, err => onAuthorized(err, req, next));
};

// Check role OR action (role name & action name must be unique)
const authRoleAction = (...rolesActions) => (context, unused, next) => {
  const { req, res } = context || {};
  permitRoleAction(...rolesActions)(req, res, err => onAuthorized(err, req, next));
};

const checkRole = (user, role) => {
  if (isObject(user) && role) {
    const { roles } = user;
    if (isObject(roles)) {
      return !!roles[role];
    }
  }
  return false;
};

const checkAdmin = user => checkRole(user, constants.roles.ADMIN);

const checkStaff = user => checkRole(user, constants.roles.STAFF);

module.exports = {
  authActions,
  authRoles,
  authRoleAction,
  checkRole,
  checkAdmin,
  checkStaff,
};

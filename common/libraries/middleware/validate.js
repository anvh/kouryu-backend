const Joi = require('joi');
const { ApiValidateError } = require('smartlog-library');
const { createError } = require('../utils/error');
const { keys: translationKeys } = require('../../i18n/resources');
const { getTranslation } = require('../utils/translation');

const doValidate = (content, schema, next) => {
  try {
    const result = Joi.validate(content, schema);
    const { error } = result;
    if (error) {
      let errorMsg;
      const { details } = error;
      const detailsNum = Array.isArray(details) ? details.length : 0;
      for (let i = 0; i < detailsNum; i += 1) {
        const { message } = details[i];
        if (message) {
          errorMsg = message;
        }
      }
      errorMsg = getTranslation(translationKeys.validateError, { message: errorMsg || error.message || `${error}` });
      next(createError(errorMsg, error.name, ApiValidateError));
    } else {
      next();
    }
  } catch (error) {
    next(error);
  }
};

const validateBody = schema => (context, unused, next) => {
  doValidate(context.req.body, schema, next);
};

const validateQuery = schema => (context, unused, next) => {
  doValidate(context.req.query, schema, next);
};

module.exports = { validateBody, validateQuery };

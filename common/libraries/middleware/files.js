const multer = require('multer');

const upload = multer({});

/**
 * hook handle single file upload
 * singleFile :: String => (Object, Any, Function) => void
 * */
const singleFile = (fieldname = 'file') => (context, unused, next) => {
  const { req, res } = context || {};
  return upload.single(fieldname)(req, res, next);
};

/**
 * hook handle multiple file upload
 * multiFiles :: String => (Object, Any, Function) => void
 */
const multiFiles = (fieldname = 'file') => (context, unused, next) => {
  const { req, res } = context || {};
  return upload.array(fieldname)(req, res, next);
};

module.exports = {
  singleFile,
  multiFiles,
};

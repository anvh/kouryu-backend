const logger = require('../../../config/logger');
const { isObject } = require('./type');

module.exports = {};
const exported = module.exports;

exported.getQueryFilter = (context) => {
  const { req } = context || {};
  const { query } = req || {};
  if (!query) {
    return undefined;
  }
  let { filter } = query;
  if (!filter) {
    filter = {};
    query.filter = filter;
    const ctx = context;
    ctx.args.filter = filter;
  }
  if (!isObject(filter)) {
    try {
      filter = JSON.parse(filter);
    } catch (error) {
      logger.error({
        message: 'getQueryFilter',
        errorInfo: `${error}`,
        errorStack: `${error.stack}`,
      });
      filter = {};
    }
    query.filter = filter;
    const ctx = context;
    ctx.args.filter = filter;
  }
  return filter;
};

exported.getQueryWhere = (context) => {
  const { req } = context || {};
  const { query } = req || {};
  if (!query) {
    return undefined;
  }
  let { where } = query;
  if (!where) {
    where = {};
    query.where = where;
    const ctx = context;
    ctx.args.where = where;
  }
  if (!isObject(where)) {
    try {
      where = JSON.parse(where);
    } catch (error) {
      logger.error({
        message: 'getQueryWhere',
        errorInfo: `${error}`,
        errorStack: `${error.stack}`,
      });
      where = {};
    }
    query.where = where;
    const ctx = context;
    ctx.args.where = where;
  }
  return where;
};

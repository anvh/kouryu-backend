const generate = require('nanoid/generate');

module.exports = () => generate('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', 15);

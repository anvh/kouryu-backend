const axios = require('axios');
const https = require('https');
const logger = require('../../../config/logger');
const { networkTimeout } = require('../../../config/config');

const convertErrorToResponse = (error) => {
  if (!error || (typeof error !== 'object')) {
    return error;
  }
  const { response } = error;
  const { data } = response || {};
  const { statusCode } = data || {};
  const status = statusCode || (response ? response.status : undefined);
  if (status) {
    let errorData = data;
    if (!errorData) {
      const { errors } = data || {};
      const errorsNum = Array.isArray(errors) ? errors.length : 0;
      for (let i = 0; i < errorsNum; i += 1) {
        errorData = errors[i];
        if (errorData) {
          break;
        }
      }
    }
    return {
      status,
      data: errorData,
    };
  }
  const errorMsg = error.message || `${error}`;
  if (errorMsg) {
    return { data: errorMsg };
  }
  return error;
};

const normalizeHeaders = (headers) => {
  const h = {};
  h.accept = 'application/json';
  const { authorization, user } = headers || {};
  if (authorization) {
    h.authorization = authorization;
  }
  if (user) {
    h.user = user;
  }
  return h;
};

const doRequest = async (method, url, body, options) => {
  if (!method || !url) {
    return undefined;
  }
  let timeoutId;
  try {
    timeoutId = setTimeout(() => {
      const isGet = method.toLowerCase() === 'get';
      const action = isGet ? 'getting' : 'sending';
      const direction = isGet ? 'from' : 'to';
      throw new Error(`Timeout while ${action} data ${direction} ${url}`);
    }, networkTimeout + 1000);
    const { headers } = options || {};
    if (options) {
      const opts = options;
      delete opts.headers;
      delete opts.httpsAgent;
      delete opts.timeout;
      delete opts.method;
      delete opts.url;
      delete opts.data;
    }
    const h = normalizeHeaders(headers);
    h['content-type'] = 'application/json';
    logger.info({
      message: 'doRequest',
      data: {
        method,
        url,
        body,
        headers,
        normalizedHeaders: h,
      },
    });
    const agent = new https.Agent({
      rejectUnauthorized: false,
    });
    const opts = Object.assign({
      headers: h,
      httpsAgent: agent,
      timeout: networkTimeout,
    }, options || {});
    const res = await axios(Object.assign({
      method,
      url,
      data: body,
    }, opts));
    return res;
  } catch (error) {
    const res = convertErrorToResponse(error);
    if (res) {
      logger.info({
        message: 'doRequest-error',
        data: {
          method,
          url,
          body,
          errorInfo: `${error}`,
          errorStack: `${error.stack}`,
          resData: res.data,
          resCode: res.status,
        },
      });
      return res;
    }
    logger.error({
      message: 'doRequest-error',
      method,
      url,
      body,
      errorInfo: `${error}`,
      errorStack: `${error.stack}`,
    });
    return undefined;
  } finally {
    if (timeoutId) {
      clearTimeout(timeoutId);
      timeoutId = undefined;
    }
  }
};

const objToQueryString = (obj) => {
  if (!obj) {
    return '';
  }
  return Object.keys(obj)
    .map((k) => {
      const val = obj[k];
      if (Array.isArray(val)) {
        return `${k}=${JSON.stringify(val)}`;
      }
      if (val) {
        return `${k}=${val}`;
      }
      return '';
    })
    .filter(item => item)
    .join('&');
};

const get = async (url, options) => {
  if (!url) {
    return undefined;
  }
  let newUrl = url;
  const { query } = options || {};
  const queryStr = objToQueryString(query);
  if (queryStr) {
    newUrl = `${url}?${queryStr}`;
    const opts = options;
    delete opts.query;
  }
  return doRequest('get', encodeURI(newUrl), undefined, options);
};

const sendJson = async (method, url, options) => {
  const { body } = options || {};
  if (body) {
    const opts = options;
    delete opts.body;
  }
  return doRequest(method, url, body, options);
};

module.exports = { doRequest, get, sendJson };

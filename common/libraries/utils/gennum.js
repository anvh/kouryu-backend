const genInt = max => Math.abs(Math.floor(Math.random() * Math.floor(max)));

module.exports = {
  genInt,
};

module.exports = {};
const exported = module.exports;

exported.isNaN = (value) => {
  if (value === 0) {
    return false;
  }
  if (!value) {
    return true;
  }
  return Number.isNaN(value);
};

exported.isObject = value => value !== null && (typeof value === 'object');

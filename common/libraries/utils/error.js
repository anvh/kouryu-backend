const createError = (errorMsg, errorName, ErrorType) => {
  const error = ErrorType ? new ErrorType() : new Error();
  error.name = errorName || 'Error';
  error.message = errorMsg;
  return error;
};

module.exports = { createError };

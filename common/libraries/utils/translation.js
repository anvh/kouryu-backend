const i18next = require('i18next');
const { translations } = require('../../i18n/resources');
const { defaultLanguageCode } = require('../../../config/config');

let i18nextInitialized;
i18next.init({
  lng: defaultLanguageCode,
  fallbackLng: defaultLanguageCode,
  resources: translations,
}, (error) => {
  if (error) {
    throw error;
  }
  i18nextInitialized = true;
});

module.exports = {};

module.exports.getTranslation = (key, options) => {
  if (!key) {
    return undefined;
  }
  if (!i18nextInitialized) {
    throw new Error('i18next has not been initialized');
  }
  const translation = options ? i18next.t(key, options) : i18next.t(key);
  return translation ? translation.replace(/&quot;/g, '\'') : translation;
};

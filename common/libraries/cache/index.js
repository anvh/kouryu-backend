const redis = require('redis');
const axios = require('axios');
const config = require('../../../config/config');

const client = redis.createClient({
  prefix: 'kouryu_cache',
  port: config.redisPort,
  host: config.redisHost,
});

const DEFAULT_LIVE_TIME = 60;

class Cache {
  constructor(liveTime) {
    this.client = client;
    this.time_to_live = liveTime || DEFAULT_LIVE_TIME;
  }

  set(key, value, liveTime = DEFAULT_LIVE_TIME) {
    if (this.client) {
      this.client.set(key, value, 'EX', liveTime);
    }
  }

  async fetchData(key) {
    if (!key || !key.startsWith('http')) {
      return null;
    }
    try {
      const result = await axios.get(key);
      this.client.set(key, JSON.stringify(result.data), 'EX', this.time_to_live);
      return result.data;
    } catch (err) {
      console.log('err', err.response.data);
      return null;
    }
  }

  get(key) {
    if (this.client) {
      return new Promise(async (resolve, reject) => {
        try {
          this.client.get(key, async (err, result) => {
            if (err || !result) {
            // fetch new data
              const data = await this.fetchData(key);
              return resolve(data);
            }
            // console.log('get from cache', JSON.stringify(result));
            return resolve(JSON.parse(result));
          });
        } catch (error) {
        // fetch new data
          const data = await this.fetchData(key);
          resolve(data);
        }
      });
    }
    return null;
  }

  getList(keys = []) {
    if (!this.client || !Array.isArray(keys)) return [];
    const self = this;
    const actionsP = keys.map(k => self.get(k));
    return Promise.all(actionsP);
  }

  delete(key) {
    if (this.client) {
      this.client.del(key);
    }
  }
}
module.exports = Cache;

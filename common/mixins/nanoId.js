const nanoid = require('nanoid');

const nanoIdLength = process.env.NANOID_LENGTH || 16;

module.exports = function (Model, options = {}) {
  options = options || {};

  Model.observe('before save', (ctx, next) => {
    const idName = options.id || Model.definition.idName();
    if (ctx.options && ctx.options.skipUpdatedAt) { return next(); }
    if (ctx.instance && !ctx.instance[idName]) {
      ctx.instance[idName] = nanoid(nanoIdLength);
    } else if (ctx.data && !ctx.data[idName]) {
      ctx.data[idName] = nanoid(nanoIdLength);
    }
    return next();
  });
};

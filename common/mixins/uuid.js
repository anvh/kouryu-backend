const uuid = require('uuid');

module.exports = function (Model, options = {}) {
  options = options || {};

  Model.observe('before save', (ctx, next) => {
    const idName = options.id || Model.definition.idName();
    if (ctx.options && ctx.options.skipUpdatedAt) { return next(); }
    if (ctx.instance && !ctx.instance[idName]) {
      ctx.instance[idName] = uuid.v4();
    } else if (ctx.data && !ctx.data[idName]) {
      ctx.data[idName] = uuid.v4();
    }
    return next();
  });
};

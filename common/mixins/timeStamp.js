module.exports = function (Model, bootOptions = {}) {
  const options = Object.assign({
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    required: true,
    validateUpsert: false, // default to turning validation off
    silenceWarnings: false,
  }, bootOptions);
  Model.defineProperty(options.createdAt, {
    type: Date,
    required: options.required,
    defaultFn: 'now',
  });

  Model.defineProperty(options.updatedAt, {
    type: Date,
    required: options.required,
  });

  Model.observe('before save', (ctx, next) => {
    if (ctx.options && ctx.options.skipUpdatedAt) { return next(); }
    if (ctx.instance) {
      // debug('%s.%s before save: %s', ctx.Model.modelName, options.updatedAt, ctx.instance.id);
      ctx.instance[options.updatedAt] = new Date();
    } else {
      // debug('%s.%s before update matching %j',
      //       ctx.Model.pluralModelName, options.updatedAt, ctx.where);
      ctx.previousData = ctx.data;
      ctx.data[options.updatedAt] = new Date();
    }
    return next();
  });
};

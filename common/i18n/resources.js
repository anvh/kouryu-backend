const validateError = 'VALIDATE_ERROR';

const accountDuplicateError = 'ACCOUNT_DUPLICATE_ERROR';
const accountPasswordTooShortError = 'ACCOUNT_PASSWORD_TOO_SHORT_ERROR';
const accountUnauthorizedError = 'ACCOUNT_UNAUTHORIZED_ERROR';
const accountNotFoundError = 'ACCOUNT_NOT_FOUND_ERROR';
const accountUnverifiedError = 'ACCOUNT_UNVERIFIED_ERROR';
const accountBlockedError = 'ACCOUNT_BLOCKED_ERROR';
const accountInfoForbiddenError = 'ACCOUNT_INFO_FORBIDDEN_ERROR';
const accountLoginMissingInfoError = 'ACCOUNT_LOGIN_MISSING_INFO_ERROR';
const accountLoginInvalidInfoError = 'ACCOUNT_LOGIN_INVALID_INFO_ERROR';
const accountStatusUpdatingForbiddenError = 'ACCOUNT_STATUS_UPDATING_FORBIDDEN_ERROR';
const accountCreateSuccess = 'ACCOUNT_CREATE_SUCCESS';
const accountUpdateSuccess = 'ACCOUNT_UPDATE_SUCCESS';
const accountDeleteSuccess = 'ACCOUNT_DELETE_SUCCESS';
const accountUpdateError = 'ACCOUNT_UPDATE_ERROR';
const emailVerificationSendError = 'EMAIL_VERIFICATION_SEND_ERROR';
const emailVerificationSubject = 'EMAIL_VERIFICATION_SUBJECT';
const emailVerificationHtml = 'EMAIL_VERIFICATION_HTML';
const verificationTokenNullError = 'VERIFICATION_TOKEN_NULL_ERROR';
const verificationTokenInvalidError = 'VERIFICATION_TOKEN_INVALID_ERROR';
const emailHasBeenSentError = 'EMAIL_HAS_BEEN_SENT_ERROR';
const emailForgotPasswordSubject = 'EMAIL_FORGOT_PASSWORD_SUBJECT';
const emailForgotPasswordHtml = 'EMAIL_FORGOT_PASSWORD_HTML';
const companyTaxNumberDuplicateError = 'COMPANY_TAX_NUMBER_DUPLICATE_ERROR';
const companyNameAdressPhoneNumberDuplicateError = 'COMPANY_NAME_ADDRESS_PHONENUMBER_DUPLICATE_ERROR';
const companyIdInvalidError = 'COMPANY_ID_INVALID_ERROR';
const newsCreateError = 'NEWS_CREATE_ERROR';
const categoryIdInvalidError = 'CATEGORY_ID_INVALID_ERROR';
const companyNoDataError = 'COMPANY_NO_DATA_ERROR';
const newsNoDataError = 'NEWS_NO_DATA_ERROR';

module.exports = {
  keys: {
    validateError,
    accountDuplicateError,
    accountPasswordTooShortError,
    accountUnauthorizedError,
    accountNotFoundError,
    accountUnverifiedError,
    accountBlockedError,
    accountInfoForbiddenError,
    accountLoginMissingInfoError,
    accountLoginInvalidInfoError,
    accountStatusUpdatingForbiddenError,
    accountCreateSuccess,
    accountUpdateSuccess,
    accountDeleteSuccess,
    accountUpdateError,
    emailVerificationSendError,
    emailVerificationSubject,
    emailVerificationHtml,
    verificationTokenNullError,
    verificationTokenInvalidError,
    emailHasBeenSentError,
    emailForgotPasswordSubject,
    emailForgotPasswordHtml,
    companyTaxNumberDuplicateError,
    companyNameAdressPhoneNumberDuplicateError,
    companyIdInvalidError,
    newsCreateError,
    categoryIdInvalidError,
    companyNoDataError,
    newsNoDataError,
  },
  translations: {
    ja: {
      translation: {
        VALIDATE_ERROR: '無効なデータ：{{message}}',
        ACCOUNT_DUPLICATE_ERROR: 'アカウントが既に存在していた。',
        ACCOUNT_PASSWORD_TOO_SHORT_ERROR: 'パスワードの長さが最短で6文字であること。',
        ACCOUNT_UNAUTHORIZED_ERROR: '管理者しか操作できない。',
        ACCOUNT_NOT_FOUND_ERROR: 'アカウント 「{{accountId}}」が存在しない。',
        ACCOUNT_UNVERIFIED_ERROR: 'アカウントがまだ確認されていない。',
        ACCOUNT_BLOCKED_ERROR: 'アカウントがブロックされている。',
        ACCOUNT_INFO_FORBIDDEN_ERROR: '他メンバーのアカウント情報が読めないこと。',
        ACCOUNT_LOGIN_MISSING_INFO_ERROR: 'ログインに必要な情報が足りない。アカウント名およびパスワードがないか確認してください。',
        ACCOUNT_LOGIN_INVALID_INFO_ERROR: 'ログインに失敗した。アカウント名およびパスワードが正しくないか確認してください。',
        ACCOUNT_STATUS_UPDATING_FORBIDDEN_ERROR: '管理者しかアカウントの状態を変更することができない。',
        ACCOUNT_CREATE_SUCCESS: 'アカウントが作成された。',
        ACCOUNT_UPDATE_SUCCESS: 'アカウントが変更された。',
        ACCOUNT_DELETE_SUCCESS: 'アカウントが削除された。',
        ACCOUNT_UPDATE_ERROR: 'An error occurred while updating account 「{{accountId}}」.',
        EMAIL_VERIFICATION_SEND_ERROR: 'Error while sending mail: {{message}}',
        EMAIL_VERIFICATION_SUBJECT: 'はやぶさのアカウントを確認する',
        EMAIL_VERIFICATION_HTML: `Dear {{email}},<br>Thank you for signing up with Hayabusa.
        <br>To provide you the best service possible, we require you to verify your email address.
        <br><a href={{link}}>Please click here to verify your email</a>
        <br>Thanks,
        <br>Hayabusa.`,
        VERIFICATION_TOKEN_NULL_ERROR: 'Token is null',
        VERIFICATION_TOKEN_INVALID_ERROR: 'Token is invalid.',
        EMAIL_HAS_BEEN_SENT_ERROR: 'Email has been sent. Please check email again.',
        EMAIL_FORGOT_PASSWORD_SUBJECT: 'Forgot password',
        EMAIL_FORGOT_PASSWORD_HTML: `Hi {{email}},<br>We were told that you forgot your password on Hayabusa.
        <br>To reset your password, please click the link. (This link will be expired in next {{time}} minutes)
        <br><a href={{link}}>Please click here to reset your password</a>
        <br>Thanks,
        <br>Hayabusa.`,
        COMPANY_TAX_NUMBER_DUPLICATE_ERROR: 'The tax number {{taxNumber}} already exists',
        COMPANY_NAME_ADDRESS_PHONENUMBER_DUPLICATE_ERROR: 'Company name, address, phone number already exists',
        COMPANY_ID_INVALID_ERROR: 'Company ID is invalid',
        NEWS_CREATE_ERROR: 'An error occurred while creating news',
        CATEGORY_ID_INVALID_ERROR: 'Category ID is invalid',
        COMPANY_NO_DATA_ERROR: 'There is no data',
        NEWS_NO_DATA_ERROR: 'There is no data',
      },
    },
    vi: {
      translation: {
        VALIDATE_ERROR: 'Dữ liệu không đúng: {{message}}',
        ACCOUNT_DUPLICATE_ERROR: 'Tài khoản đã tồn tại',
        ACCOUNT_PASSWORD_TOO_SHORT_ERROR: 'Mật khẩu phải dài ít nhất 6 ký tự',
        ACCOUNT_UNAUTHORIZED_ERROR: ' Chỉ người quản lý mới có thể thao tác',
        ACCOUNT_NOT_FOUND_ERROR: 'Tài khoản "{{accountId}}" không tồn tại',
        ACCOUNT_UNVERIFIED_ERROR: 'Tài khoản chưa được xác thực',
        ACCOUNT_BLOCKED_ERROR: 'Tài khoản đã bị khóa',
        ACCOUNT_INFO_FORBIDDEN_ERROR: 'Bạn không thể xem thông tin tài khoản của người khác',
        ACCOUNT_LOGIN_MISSING_INFO_ERROR: 'Không đủ thông tin để đăng nhập, vui lòng kiểm tra lại tên tài khoản và mật khẩu đã có hay chưa',
        ACCOUNT_LOGIN_INVALID_INFO_ERROR: 'Đăng nhập không thành công, vui lòng kiểm tra lại tên tài khoản và mật khẩu đã đúng hay chưa',
        ACCOUNT_STATUS_UPDATING_FORBIDDEN_ERROR: 'Chỉ người quản lý mới có thể thay đổi trạng thái của tài khoản',
        ACCOUNT_CREATE_SUCCESS: 'Tài khoản được tạo thành công',
        ACCOUNT_UPDATE_SUCCESS: 'Tài khoản đã được cập nhật thành công',
        ACCOUNT_DELETE_SUCCESS: 'Tài khoản đã được xóa thành công',
        ACCOUNT_UPDATE_ERROR: 'Đã có lỗi xảy ra trong quá trình cập nhật tài khoản 「{{accountId}}」。',
        EMAIL_VERIFICATION_SEND_ERROR: 'Đã có lỗi trong quá trình gửi email: {{message}}',
        EMAIL_VERIFICATION_SUBJECT: 'Xác nhận tài khoản của bạn trên Hayabusa',
        EMAIL_VERIFICATION_HTML: `Thân gửi {{email}},<br>Cảm ơn bạn đã đăng ký tài khoản với Hayabusa.
        <br>Để cung cấp cho bạn dịch vụ tốt nhất có thể, chúng tôi cần bạn xác thực tài khoản email của bạn.
        <br><a href={{link}}>Vui lòng nhấn vào đây để xác thực email</a>
        <br>Thân,
        <br>Hayabusa.`,
        VERIFICATION_TOKEN_NULL_ERROR: 'Token không có giá trị',
        VERIFICATION_TOKEN_INVALID_ERROR: 'Token không hợp lệ, vui lòng kiểm tra lại email xác thực',
        EMAIL_HAS_BEEN_SENT_ERROR: 'Email đã được gửi. Vui lòng kiểm tra lại hộp thư.',
        EMAIL_FORGOT_PASSWORD_SUBJECT: 'Quên mật khẩu',
        EMAIL_FORGOT_PASSWORD_HTML: `Chào {{email}},<br>Chúng tôi được thông báo rằng bạn đã quên mật khẩu tài khoản trên Hayabusa.
        <br>Để thiết lập lại mật khẩu, hãy nhấn vào đường dẫn sau. (Đường dẫn sẽ hết hạn sau {{time}} phút nữa)
        <br><a href={{link}}>Vui lòng nhấn vào đây để thiết lập lại mật khẩu</a>
        <br>Thân,
        <br>Hayabusa.`,
        COMPANY_TAX_NUMBER_DUPLICATE_ERROR: 'Đã tồn tại mã số thuế {{taxNumber}}',
        COMPANY_NAME_ADDRESS_PHONENUMBER_DUPLICATE_ERROR: 'Tên công ty, địa chỉ, số điện thoại đã tồn tại',
        COMPANY_ID_INVALID_ERROR: 'Company ID không có giá trị',
        NEWS_CREATE_ERROR: 'Đã có lỗi xảy ra trong quá trình tạo bảng tin',
        CATEGORY_ID_INVALID_ERROR: 'Category ID không có giá trị',
        COMPANY_NO_DATA_ERROR: 'Không có dữ liệu',
        NEWS_NO_DATA_ERROR: 'Không có dữ liệu',
      },
    },
    en: {
      translation: {
        VALIDATE_ERROR: 'Invalid data: {{message}}',
        ACCOUNT_DUPLICATE_ERROR: 'Another account with the same username exists',
        ACCOUNT_PASSWORD_TOO_SHORT_ERROR: 'Password should be at least 6 characters long',
        ACCOUNT_UNAUTHORIZED_ERROR: 'Only the admin can perform the operation',
        ACCOUNT_UNVERIFIED_ERROR: 'The account hasn\'t been verified',
        ACCOUNT_BLOCKED_ERROR: 'The account has been blocked',
        ACCOUNT_INFO_FORBIDDEN_ERROR: 'It\'s impposible to view details of an account belonging to another user',
        ACCOUNT_NOT_FOUND_ERROR: 'Account "{{accountId}}" does not exist',
        ACCOUNT_LOGIN_MISSING_INFO_ERROR: 'Login information is not enough, please check if the username and password are not missing',
        ACCOUNT_LOGIN_INVALID_INFO_ERROR: 'Login failed, please check if the username and password are correct',
        ACCOUNT_STATUS_UPDATING_FORBIDDEN_ERROR: 'The account\'s status can only be changed by an admin',
        ACCOUNT_CREATE_SUCCESS: 'Account created successfully',
        ACCOUNT_UPDATE_SUCCESS: 'Account updated successfully',
        ACCOUNT_DELETE_SUCCESS: 'Account deleted successfully',
        ACCOUNT_UPDATE_ERROR: 'An error occurred while updating account 「{{accountId}}」.',
        EMAIL_VERIFICATION_SEND_ERROR: 'Error while sending mail: {{message}}',
        EMAIL_VERIFICATION_SUBJECT: 'Confirm your account on Hayabusa',
        EMAIL_VERIFICATION_HTML: `Dear {{email}},<br>Thank you for signing up with Hayabusa.
        <br>To provide you the best service possible, we require you to verify your email address.
        <br><a href={{link}}>Please click here to verify your email</a>
        <br>Thanks,
        <br>Hayabusa.`,
        VERIFICATION_TOKEN_NULL_ERROR: 'Token is null',
        VERIFICATION_TOKEN_INVALID_ERROR: 'Token is invalid, please check varification link in your email.',
        EMAIL_HAS_BEEN_SENT_ERROR: 'Email has been sent. Please check email again.',
        EMAIL_FORGOT_PASSWORD_SUBJECT: 'Forgot password',
        EMAIL_FORGOT_PASSWORD_HTML: `Hi {{email}},<br>We were told that you forgot your password on Hayabusa.
        <br>To reset your password, please click the link. (This link will be expired in next {{time}} minutes)
        <br><a href={{link}}>Please click here to reset your password</a>
        <br>Thanks,
        <br>Hayabusa.`,
        COMPANY_TAX_NUMBER_DUPLICATE_ERROR: 'The tax number {{taxNumber}} already exists',
        COMPANY_NAME_ADDRESS_PHONENUMBER_DUPLICATE_ERROR: 'Company name, address, phone number already exists',
        COMPANY_ID_INVALID_ERROR: 'Company ID is invalid',
        NEWS_CREATE_ERROR: 'An error occurred while creating news',
        CATEGORY_ID_INVALID_ERROR: 'Category ID is invalid',
        COMPANY_NO_DATA_ERROR: 'There is no data',
        NEWS_NO_DATA_ERROR: 'There is no data',
      },
    },
  },
};

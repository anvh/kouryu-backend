module.exports = {
  actions: {
    READ_ACCOUNTS: 'read_accounts',
    READ_ACCOUNT: 'read_account',
    CREATE_STAFF_ACCOUNT: 'create_staff_account',
    CREATE_ADMIN_ACCOUNT: 'create_admin_account',
    UPDATE_ACCOUNT: 'update_account',
    DELETE_ACCOUNT: 'delete_account',

    READ_COMPANIES: 'read_companies',
    READ_COMPANY: 'read_company',
    CREATE_COMPANY: 'create_company',
    UPDATE_COMPANY: 'update_company',
    DELETE_COMPANY: 'delete_company',

    CREATE_NEWS: 'create_news',
    UPDATE_NEWS: 'update_news',
    DELETE_NEWS: 'delete_news',
  },
  roles: {
    ADMIN: 'admin',
    STAFF: 'staff',
    USER: 'user',
  },
  accountStatuses: {
    UNVERIFIED: 'unverified',
    VERIFIED: 'verified',
    BLOCKED: 'blocked',
  },
  verificationTypes: {
    VERIFICATION: 'verification',
    FORGOTPASSWORD: 'forgotpassword',
  },
  companyStatuses: {
    UNVERIFIED: 'unverified',
    VERIFIED: 'verified',
    BLOCKED: 'blocked',
    DELETED: 'deleted',
  },
  newsStatuses: {
    PRIVATE: 'private',
    PUBLIC: 'public',
    BLOCKED: 'blocked',
    DELETED: 'deleted',
  },
};
